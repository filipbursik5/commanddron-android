package cz.bursifil.commanddrone.core.main.presentation;

import org.junit.Before;
import org.junit.Test;

import cz.bursifil.commanddrone.core.main.application.joystick.JoystickController;
import cz.bursifil.commanddrone.core.main.domain.MainStrings;
import cz.bursifil.commanddrone.core.main.presentation.joystick.JoystickPresenter;
import cz.bursifil.commanddrone.core.main.presentation.joystick.JoystickPresenterImpl;
import cz.bursifil.commanddrone.core.main.presentation.joystick.JoystickView;

import static org.junit.Assert.assertEquals;

/**
 * Created by filas on 3/6/2018.
 */
public class JoystickPresenterImplTest {

    private JoystickPresenter sut;
    private JoystickController joystickControllerDub;
    private JoystickViewSpy joystickViewSpy;

    @Before
    public void setUp() {
        joystickViewSpy = new JoystickViewSpy();
        joystickControllerDub = new JoystickControllerDub();
        sut = new JoystickPresenterImpl(joystickViewSpy, joystickControllerDub);
    }

    @Test
    public void test_WhenExeceuteCommandCalledOnDisconnectedSSH_ThenErrorCalled() {
        sut.setJoystickChange(0);

        assertEquals(joystickViewSpy.error, "test");
    }

    class JoystickViewSpy implements JoystickView {

        public boolean setupDataCalled = false;
        public MainStrings mainStrings;
        public String error;

        @Override
        public void setupData(MainStrings mainStrings) {
            setupDataCalled = true;
            this.mainStrings = mainStrings;
        }

        @Override
        public void commandError(String text) {
            this.error = text;
        }
    }

    class JoystickControllerDub implements JoystickController {

        @Override
        public void executeJoystickCommand(String command, PresenterCallback presenterCallback) {
            presenterCallback.error("test");
        }
    }
}