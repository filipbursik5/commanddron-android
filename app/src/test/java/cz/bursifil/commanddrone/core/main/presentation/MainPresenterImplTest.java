package cz.bursifil.commanddrone.core.main.presentation;

import org.junit.Before;
import org.junit.Test;

import cz.bursifil.commanddrone.core.main.domain.MainStrings;

import static org.junit.Assert.*;

/**
 * Created by filas on 3/6/2018.
 */
public class MainPresenterImplTest {

    private MainPresenter sut;
    private StringServiceDub stringServiceDub;
    private MainViewSpy mainViewSpy;

    @Before
    public void setUp(){
        mainViewSpy = new MainViewSpy();
        stringServiceDub = new StringServiceDub();
        sut = new MainPresenterImpl(mainViewSpy, stringServiceDub);
    }

    @Test
    public void test_WhenViewLoads_ThenViewSetupData(){
        sut.onCreate();

        assertEquals(mainViewSpy.setupDataCalled, true);
    }

    @Test
    public void test_WhenViewLoads_ThenViewSetupedDataAreCorrect(){
        sut.onCreate();

        assertEquals(mainViewSpy.mainStrings.getCommandsTitle(), "test");
        assertEquals(mainViewSpy.mainStrings.getConsoleTitle(), "test");
        assertEquals(mainViewSpy.mainStrings.getFollowTitle(), "test");
        assertEquals(mainViewSpy.mainStrings.getJoystickTitle(), "test");
        assertEquals(mainViewSpy.mainStrings.getNavBartitle(), "test");
    }

    class MainViewSpy implements MainView {

        public boolean setupDataCalled = false;
        public MainStrings mainStrings;

        @Override
        public void setupData(MainStrings mainStrings) {
            setupDataCalled = true;
            this.mainStrings = mainStrings;
        }

        @Override
        public void deviceConnected(String text) {

        }

        @Override
        public void deviceDisconnected(String text) {

        }
    }

    class StringServiceDub implements cz.bursifil.commanddrone.phone.platform.StringService {

        @Override
        public String getStringById(Integer id) {
            return "test";
        }
    }
}