package cz.bursifil.commanddrone.core.saveddevices.presentation;

import cz.bursifil.commanddrone.core.saveddevices.domain.Device;

/**
 * Created by filas on 3/6/2018.
 */

public interface SavedDevicesPresenter {
    void onCreate();

    void startConnecting(Device device);
    void deleteDevice(Integer position);
}

