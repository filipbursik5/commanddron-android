package cz.bursifil.commanddrone.core.main.domain;

/**
 * Created by filas on 3/6/2018.
 */

public class CommandStrings {

    public String navBartitle;
    public String consoleTitle;
    public String commandsTitle;
    public String followTitle;
    public String joystickTitle;

    public CommandStrings(String navBartitle, String consoleTitle, String commandsTitle, String followTitle, String joystickTitle) {
        this.navBartitle = navBartitle;
        this.consoleTitle = consoleTitle;
        this.commandsTitle = commandsTitle;
        this.followTitle = followTitle;
        this.joystickTitle = joystickTitle;
    }

    public String getNavBartitle() {
        return navBartitle;
    }

    public String getConsoleTitle() {
        return consoleTitle;
    }

    public String getCommandsTitle() {
        return commandsTitle;
    }

    public String getFollowTitle() {
        return followTitle;
    }

    public String getJoystickTitle() {
        return joystickTitle;
    }

    public void setNavBartitle(String navBartitle) {
        this.navBartitle = navBartitle;
    }

    public void setConsoleTitle(String consoleTitle) {
        this.consoleTitle = consoleTitle;
    }

    public void setCommandsTitle(String commandsTitle) {
        this.commandsTitle = commandsTitle;
    }

    public void setFollowTitle(String followTitle) {
        this.followTitle = followTitle;
    }

    public void setJoystickTitle(String joystickTitle) {
        this.joystickTitle = joystickTitle;
    }
}
