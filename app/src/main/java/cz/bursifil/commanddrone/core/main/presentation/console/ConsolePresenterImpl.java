package cz.bursifil.commanddrone.core.main.presentation.console;

import android.text.Editable;

import javax.inject.Inject;

import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.Constants;
import cz.bursifil.commanddrone.core.PresenterCallback;
import cz.bursifil.commanddrone.core.main.application.console.ConsoleController;
import cz.bursifil.commanddrone.phone.root.DronApplication;

/**
 * Created by filas on 3/6/2018.
 */

public class ConsolePresenterImpl implements ConsolePresenter, PresenterCallback
{
    private ConsoleView view;
    private ConsoleController consoleController;

    @Inject
    public ConsolePresenterImpl(ConsoleView consoleView, ConsoleController consoleController) {
        this.view = consoleView;
        this.consoleController = consoleController;
    }

    public void onCreate(){
    }

    public void perform(Editable command) {
        consoleController.executeCommand(command.toString(), this);
    }

    @Override
    public String getFormattedCommand(Editable editable) {
        return DronApplication.getsContext().getString(R.string.sendcommand) + ": " + editable.toString() + Constants.NEW_LINE;
    }

    @Override
    public void success(String data) {
        view.addToConsole(DronApplication.getsContext().getString(R.string.recieved) + ": " + data + Constants.NEW_LINE);
    }

    @Override
    public void error(String data) {
        view.addErrorToConsole(DronApplication.getsContext().getString(R.string.wrong) + Constants.NEW_LINE);
    }
}
