package cz.bursifil.commanddrone.core.main.application.joystick;

import com.jcraft.jsch.JSchException;

import java.io.IOException;

import cz.bursifil.commanddrone.core.PresenterCallback;
import cz.cvut.fel.androidsshcommunication.sshutils.CommandCallback;
import cz.cvut.fel.androidsshcommunication.sshutils.CommandController;

/**
 * Created by filas on 3/22/2018.
 */

public class JoystickControllerImpl implements JoystickController, CommandCallback {

    private PresenterCallback presenterCallback;

    public void executeJoystickCommand(String command, PresenterCallback presenterCallback) {
        this.presenterCallback = presenterCallback;

        CommandController.executeCommand(command, this);
    }

    @Override
    public void setResult(String result) {

    }
}
