package cz.bursifil.commanddrone.core.main.presentation.command;

/**
 * Created by filas on 3/6/2018.
 */

public interface CommandPresenter {
    void onCreate();
    void goToAltitude(String altitude);
    void goToRelative(String x, String y, String z, String tilt);
    void goTo(String x, String y, String z, String tilt);
}

