package cz.bursifil.commanddrone.core.customcamera.application;

import com.jcraft.jsch.JSchException;

import java.io.IOException;

import cz.bursifil.commanddrone.core.PresenterCallback;
import cz.cvut.fel.androidsshcommunication.sshutils.CommandCallback;
import cz.cvut.fel.androidsshcommunication.sshutils.CommandController;

/**
 * Created by filas on 3/22/2018.
 */

public class CustomCameraController implements CommandCallback {

    private PresenterCallback presenterCallback;

    public void executeFollowCommand(String command, PresenterCallback presenterCallback){
        this.presenterCallback = presenterCallback;

        CommandController.executeCommand(command, this);
    }

    @Override
    public void setResult(String result) {

    }
}
