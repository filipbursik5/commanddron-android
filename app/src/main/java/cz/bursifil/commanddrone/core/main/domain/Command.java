package cz.bursifil.commanddrone.core.main.domain;

/**
 * Created by filas on 12/4/2017.
 */

public class Command {

    private String name;
    private String command;

    public Command(String name, String command) {
        this.name = name;
        this.command = command;
    }

    public String getName() {
        return name;
    }

    public String getCommand() {
        return command;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCommand(String command) {
        this.command = command;
    }
}
