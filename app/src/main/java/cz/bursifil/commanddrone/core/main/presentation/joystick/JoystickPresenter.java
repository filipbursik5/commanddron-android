package cz.bursifil.commanddrone.core.main.presentation.joystick;

/**
 * Created by filas on 3/6/2018.
 */

public interface JoystickPresenter {
    void onCreate();
    void setJoystickChange(double angle);
}

