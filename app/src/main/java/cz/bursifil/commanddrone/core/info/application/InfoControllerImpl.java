package cz.bursifil.commanddrone.core.info.application;

import com.jcraft.jsch.JSchException;

import java.io.IOException;

import cz.bursifil.commanddrone.core.PresenterCallback;
import cz.cvut.fel.androidsshcommunication.sshutils.CommandCallback;
import cz.cvut.fel.androidsshcommunication.sshutils.CommandController;

/**
 * Created by filas on 3/6/2018.
 */

public class InfoControllerImpl implements InfoController, CommandCallback {

    private final String COMMAND = "roslaunch battery battery.launch";
    private PresenterCallback presenterCallback;

    @Override
    public void getBattery(PresenterCallback presenterCallback) {

        this.presenterCallback = presenterCallback;

        CommandController.executeCommand(COMMAND, this);
    }

    @Override
    public void setResult(String result) {
        String s = result.substring(result.indexOf("(") + 1);
        s = s.substring(0, s.indexOf(")"));

        String s2 = result.substring(result.indexOf("{") + 1);
        s2 = s2.substring(0, s2.indexOf("}"));

        presenterCallback.success(s + "  " + s2);
    }
}
