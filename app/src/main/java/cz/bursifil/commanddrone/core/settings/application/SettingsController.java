package cz.bursifil.commanddrone.core.settings.application;

import cz.bursifil.commanddrone.core.PresenterCallback;

/**
 * Created by filas on 3/6/2018.
 */

public interface SettingsController {
    void saveDevice(String hostname, String username, String password);
    void disconnect();
    void connect(String hostname, String username, String password, PresenterCallback presenterCallback);
    boolean isConnected();
}
