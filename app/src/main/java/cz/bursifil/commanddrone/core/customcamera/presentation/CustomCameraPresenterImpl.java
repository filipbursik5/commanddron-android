package cz.bursifil.commanddrone.core.customcamera.presentation;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;

import com.example.globalsettings.GlobalSettings;

import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.PresenterCallback;
import cz.bursifil.commanddrone.core.customcamera.application.CustomCameraController;
import cz.bursifil.commanddrone.core.main.application.follow.FollowController;
import cz.bursifil.commanddrone.core.main.domain.UTM;
import cz.bursifil.commanddrone.core.main.presentation.follow.FollowPresenterImpl;
import cz.bursifil.commanddrone.phone.platform.GPSUtility;
import cz.bursifil.commanddrone.phone.root.DronApplication;

/**
 * Created by filas on 3/6/2018.
 */

public class CustomCameraPresenterImpl implements CustomCameraPresenter, PresenterCallback {
    private CustomCameraView view;

    private boolean followingInd = false;
    private Double bearing;
    private int seekBarProgress;

    private final Double[] A = new Double[1];
    private final Double[] B = new Double[1];
    private final Double[] C = new Double[1];

    final float[] mValuesMagnet = new float[3];
    final float[] mValuesAccel = new float[3];
    final float[] mValuesOrientation = new float[3];
    final float[] mRotationMatrix = new float[9];
    private Timer timerTask = new Timer();
    private Double myHeigh = 1.7;

    private CustomCameraController controller;
    private GPSUtility gpsUtility;

    @Inject
    public CustomCameraPresenterImpl(CustomCameraView followView, CustomCameraController customCameraController, GPSUtility gpsUtility) {
        this.view = followView;
        this.controller = customCameraController;
        this.gpsUtility = gpsUtility;
    }

    public void onCreate() {
    }

    public void setSeekBarProgress(int seekBarProgress) {
        this.seekBarProgress = seekBarProgress;
    }

    @Override
    public void setBearing(Double bearing) {
        this.bearing = bearing;
    }

    @Override
    public void setValuesAccelerometr(float[] mValuesAccelerometr) {
        System.arraycopy(mValuesAccelerometr, 0, mValuesAccel, 0, 3);
    }

    @Override
    public void setValuesMagnetic(float[] mValuesMagnetic) {
        System.arraycopy(mValuesMagnetic, 0, mValuesMagnet, 0, 3);
    }

    @Override
    public void follow() {

        timerTask.schedule(new TimerTask() {
            @Override
            public void run() {
                float[] temp = new float[9];
                float[] RR = new float[9];
                float[] orientation = new float[3];
                float[] rotation = new float[9];

                float[] mOriginValuesAccel = mValuesAccel.clone();
                float[] mOriginValuesMagnet = mValuesMagnet.clone();

                SensorManager.getRotationMatrix(rotation, null, mOriginValuesAccel, mOriginValuesMagnet);
                SensorManager.getOrientation(rotation, orientation);

                SensorManager.getRotationMatrix(temp, null,
                        mOriginValuesAccel, mOriginValuesMagnet);

                SensorManager.remapCoordinateSystem(temp,
                        SensorManager.AXIS_X,
                        SensorManager.AXIS_Y, RR);

                float[] values = new float[3];
                SensorManager.getOrientation(RR, values);

                Double degrees = (values[2] * 180) / Math.PI;

                degrees += 90;

                if (degrees > 0) {
                    degrees -= 360;
                }

                degrees *= -1;

                if (bearing < 0) {
                    bearing += 360;
                }

                Double beta = Math.PI / 180 * degrees;
                Double aplha = Math.PI / 180 * bearing;

                A[0] = seekBarProgress * Math.cos(beta) * Math.cos(aplha);
                C[0] = seekBarProgress * Math.sin(beta);
                B[0] = seekBarProgress * Math.sin(aplha) * Math.cos(beta);

                Location myLocation = gpsUtility.getLocation();
                Location movedLocation = gpsUtility.moveGps(bearing, seekBarProgress, myLocation);
                UTM utmCoordinates = gpsUtility.Deg2UTM(movedLocation);
                utmCoordinates.setZ(myHeigh + Math.sin(beta) * seekBarProgress);

                view.setInfo(degrees.toString(), bearing.toString(), new Double(utmCoordinates.getX()).toString(),
                        new Double(utmCoordinates.getY()).toString(), new Double(utmCoordinates.getZ()).toString());

                String command = "rosservice call /uav" + GlobalSettings.UAV_NUMBER + "/trackers_manager/mpc_tracker/goTo \"goal:\n- " + utmCoordinates.getX() + "\n- "
                        + utmCoordinates.getY() + "\n- " + utmCoordinates.getZ() + "\n- " + "0" + "\"";

                /*String command = "rosservice call /uav" + GlobalSettings.UAV_NUMBER + "/trackers_manager/mpc_tracker/goTo \"goal:\n- " + A[0] + "\n- "
                        + B[0] + "\n- " + C[0] + "\n- " + "0" + "\"";*/

                controller.executeFollowCommand(command, CustomCameraPresenterImpl.this);
            }
        }, 100, 1000);
    }

    @Override
    public void stopFollow(){
        timerTask.cancel();
        timerTask.purge();
    }

    @Override
    public void error(String text) {

    }

    @Override
    public void success(String text) {

    }
}
