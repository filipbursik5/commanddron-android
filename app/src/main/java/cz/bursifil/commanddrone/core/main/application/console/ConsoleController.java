package cz.bursifil.commanddrone.core.main.application.console;

import android.util.Log;

import com.jcraft.jsch.JSchException;

import java.io.IOException;

import cz.bursifil.commanddrone.core.PresenterCallback;
import cz.bursifil.commanddrone.core.main.domain.Command;
import cz.cvut.fel.androidsshcommunication.sshutils.CommandCallback;
import cz.cvut.fel.androidsshcommunication.sshutils.CommandController;

/**
 * Created by filas on 3/22/2018.
 */

public class ConsoleController implements CommandCallback {

    private PresenterCallback presenterCallback;

    public void executeCommand(String command, PresenterCallback presenterCallback) {
        this.presenterCallback = presenterCallback;

        long lastTime = System.nanoTime() * 1000000;
            CommandController.executeCommand(command, this);

            long executionTime = System.nanoTime() * 1000000 - lastTime;

            Log.e("EXECUTINGTIME", new Long(executionTime).toString() + "ms");
    }

    @Override
    public void setResult(String result) {
        presenterCallback.success(result);
    }
}
