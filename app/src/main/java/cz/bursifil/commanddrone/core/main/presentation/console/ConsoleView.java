package cz.bursifil.commanddrone.core.main.presentation.console;

import cz.bursifil.commanddrone.core.main.domain.MainStrings;

/**
 * Created by filas on 3/6/2018.
 */

public interface ConsoleView {
    void setupData(MainStrings mainStrings);
    void addToConsole(String text);
    void addErrorToConsole(String string);
}
