package cz.bursifil.commanddrone.core.main.presentation.joystick;

import com.example.globalsettings.GlobalSettings;

import javax.inject.Inject;

import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.PresenterCallback;
import cz.bursifil.commanddrone.core.main.application.joystick.JoystickController;
import cz.bursifil.commanddrone.phone.root.DronApplication;

/**
 * Created by filas on 3/6/2018.
 */

public class JoystickPresenterImpl implements JoystickPresenter, PresenterCallback
{
    private JoystickView view;
    private JoystickController controller;

    long deltaTime = System.currentTimeMillis() / 1000;

    @Inject
    public JoystickPresenterImpl(JoystickView joystickView, JoystickController joystickControllerImpl) {
        this.controller = joystickControllerImpl;
        this.view = joystickView;
    }

    public void onCreate(){
    }

    @Override
    public void setJoystickChange(double angle) {
        if((double)System.currentTimeMillis() / 1000 - (double)deltaTime < 2){
            return;
        }

        String command;
        if (angle > 0 && angle < 90) {
            command = "rosservice call /uav" + GlobalSettings.UAV_NUMBER + "/trackers_manager/mpc_tracker/goToRelative \"goal:\n- " + "2" + "\n- "
                    + "0" + "\n- " + "0" + "\n- " + "0" + "\"";
        } else if (angle > 90 && angle < 180) {
            command = "rosservice call /uav" + GlobalSettings.UAV_NUMBER + "/trackers_manager/mpc_tracker/goToRelative \"goal:\n- " + "0" + "\n- "
                    + "2" + "\n- " + "0" + "\n- " + "0" + "\"";
        } else if (angle > 180 && angle < 270) {
            command = "rosservice call /uav" + GlobalSettings.UAV_NUMBER + "/trackers_manager/mpc_tracker/goToRelative \"goal:\n- " + "-2" + "\n- "
                    + "0" + "\n- " + "0" + "\n- " + "0" + "\"";
        } else {
            command = "rosservice call /uav" + GlobalSettings.UAV_NUMBER + "/trackers_manager/mpc_tracker/goToRelative \"goal:\n- " + "0" + "\n- "
                    + "-2" + "\n- " + "0" + "\n- " + "0" + "\"";
        }

        controller.executeJoystickCommand(command, this);

        deltaTime = System.currentTimeMillis() / 1000;
    }

    @Override
    public void success(String data) {

    }

    @Override
    public void error(String data) {
        view.commandError(DronApplication.getsContext().getString(R.string.wrong));
    }
}
