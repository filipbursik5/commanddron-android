package cz.bursifil.commanddrone.core.main.presentation;

import javax.inject.Inject;

import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.main.domain.MainStrings;
import cz.bursifil.commanddrone.phone.platform.StringService;
import cz.cvut.fel.androidsshcommunication.sshutils.SessionController;

/**
 * Created by filas on 3/6/2018.
 */

public class MainPresenterImpl implements MainPresenter
{
    private MainView view;
    private StringService stringServiceImpl;

    @Inject
    public MainPresenterImpl(MainView mainView, StringService stringServiceImpl) {
        this.view = mainView;
        this.stringServiceImpl = stringServiceImpl;
    }

    public void onCreate() {
        MainStrings mainStrings = new MainStrings(stringServiceImpl.getStringById(R.string.app_name),
                stringServiceImpl.getStringById(R.string.commands),
                stringServiceImpl.getStringById(R.string.console),
                stringServiceImpl.getStringById(R.string.follow),
                stringServiceImpl.getStringById(R.string.joystick)
        );

        view.setupData(mainStrings);
    }

    public void onResume(){

    if(SessionController.isConnected()){
        view.deviceConnected(stringServiceImpl.getStringById(R.string.connected));
    }else{
        view.deviceDisconnected(stringServiceImpl.getStringById(R.string.notconnected));
    }

    }
}
