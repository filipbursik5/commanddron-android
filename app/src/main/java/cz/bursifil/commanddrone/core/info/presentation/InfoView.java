package cz.bursifil.commanddrone.core.info.presentation;

import cz.bursifil.commanddrone.core.info.domain.InfoStrings;

/**
 * Created by filas on 3/6/2018.
 */

public interface InfoView {
    void setupData(InfoStrings infoStrings);
    void showBattery(String battery);
    void showVoltage(String voltage);
}
