package cz.bursifil.commanddrone.core.info.presentation;

import javax.inject.Inject;

import cz.bursifil.commanddrone.core.PresenterCallback;
import cz.bursifil.commanddrone.core.info.application.InfoController;

/**
 * Created by filas on 3/6/2018.
 */

public class InfoPresenterImpl implements InfoPresenter, PresenterCallback
{
    private InfoView view;
    private InfoController infoController;

    @Inject
    public InfoPresenterImpl(InfoView infoView, InfoController infoController) {
        this.view = infoView;
        this.infoController = infoController;
    }

    public void onCreate(){
       infoController.getBattery(this);
    }

    @Override
    public void success(String data) {
        String[] voltageAndBattery = data.split("  ");
        view.showBattery(voltageAndBattery[0]);
        view.showVoltage(voltageAndBattery[1]);
    }

    @Override
    public void error(String data) {

    }
}
