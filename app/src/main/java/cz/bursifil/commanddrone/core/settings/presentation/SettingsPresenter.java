package cz.bursifil.commanddrone.core.settings.presentation;

/**
 * Created by filas on 3/6/2018.
 */

public interface SettingsPresenter {
    void onCreate();
    void saveDevice();
    void saveCreditionals(String hostname, String password, String username);
    void disconnect();
}

