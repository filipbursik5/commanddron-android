package cz.bursifil.commanddrone.core.customcamera.presentation;

/**
 * Created by filas on 3/6/2018.
 */

public interface CustomCameraPresenter {
    void onCreate();
    void follow();
    void setBearing(Double bearing);
    void setValuesAccelerometr(float[] mValuesAccelerometr);
    void setValuesMagnetic(float[] mValuesMagnetic);
    void setSeekBarProgress(int seekBarProgress);
    void stopFollow();
}

