package cz.bursifil.commanddrone.core.saveddevices.domain;

import java.io.Serializable;

/**
 * Created by filipbursik on 06.12.17.
 */

public class Device implements Serializable {

    private String username;
    private String hostname;
    private String password;

    public Device(String username, String hostname, String password) {
        this.username = username;
        this.hostname = hostname;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getHostname() {
        return hostname;
    }

    public String getPassword() {
        return password;
    }
}
