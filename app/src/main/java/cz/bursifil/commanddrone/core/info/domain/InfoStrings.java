package cz.bursifil.commanddrone.core.info.domain;

/**
 * Created by filas on 3/6/2018.
 */

public class InfoStrings {

    public String navBartitle;

    public InfoStrings(String navBartitle) {
        this.navBartitle = navBartitle;
    }

    public String getNavBartitle() {
        return navBartitle;
    }

    public void setNavBartitle(String navBartitle) {
        this.navBartitle = navBartitle;
    }
}
