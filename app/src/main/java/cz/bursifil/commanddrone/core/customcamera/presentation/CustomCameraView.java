package cz.bursifil.commanddrone.core.customcamera.presentation;

/**
 * Created by filas on 3/6/2018.
 */

public interface CustomCameraView {
    void setInfo(String alpha, String beta, String x, String y, String z);
}
