package cz.bursifil.commanddrone.core.main.presentation.command;

import java.util.List;

import cz.bursifil.commanddrone.core.main.domain.Command;
import cz.bursifil.commanddrone.core.main.domain.CommandStrings;

/**
 * Created by filas on 3/6/2018.
 */

public interface CommandView {
    void setupData(CommandStrings commandStrings);
    void showCommands(List<Command> commands);
    void setErrorConnection(String text);
}
