package cz.bursifil.commanddrone.core.main.presentation.console;

import android.text.Editable;

/**
 * Created by filas on 3/6/2018.
 */

public interface ConsolePresenter {
    void onCreate();
    void perform(Editable command);
    String getFormattedCommand(Editable editable);
}

