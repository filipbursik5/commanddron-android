package cz.bursifil.commanddrone.core.main.presentation.command;

import android.location.Location;

import com.example.globalsettings.GlobalSettings;
import com.jcraft.jsch.JSchException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.main.domain.Command;
import cz.bursifil.commanddrone.phone.platform.GPSUtility;
import cz.bursifil.commanddrone.phone.root.DronApplication;
import cz.cvut.fel.androidsshcommunication.sshutils.CommandCallback;
import cz.cvut.fel.androidsshcommunication.sshutils.CommandController;

/**
 * Created by filas on 3/6/2018.
 */

public class CommandPresenterImpl implements CommandPresenter, CommandCallback
{
    private CommandView view;

    private String goToCommand = "rosservice call /uav" + GlobalSettings.UAV_NUMBER + "/trackers_manager/mpc_tracker/goTo \"goal:\n";
    private String goToAltitudeCommand = "rosservice call /uav" + GlobalSettings.UAV_NUMBER + "/trackers_manager/mpc_tracker/goToAltitude \"goal: ";
    private String goToRelativeCommand = "rosservice call /uav" + GlobalSettings.UAV_NUMBER + "/trackers_manager/mpc_tracker/goToRelative \"goal:\n";

    @Inject
    public CommandPresenterImpl(CommandView commandView) {
        this.view = commandView;
    }

    public void onCreate(){
        view.showCommands(getCommands());
    }

    @Override
    public void goToAltitude(String altitude) {
        String command = goToAltitudeCommand  + altitude + "\"";

        executeCommandWithErrorHandler(command);
    }

    @Override
    public void goToRelative(String x, String y, String z, String tilt) {
        String command = goToRelativeCommand  +
                "- " + x + "\n" +
                "- " + y + "\n" +
                "- " + z + "\n" +
                "- " + tilt + "\"\n";

        executeCommandWithErrorHandler(command);
    }

    @Override
    public void goTo(String x, String y, String z, String tilt) {
        String command = goToCommand  +
                "- " + x + "\n" +
                "- " + y + "\n" +
                "- " + z + "\n" +
                "- " + tilt + "\"\n";

        executeCommandWithErrorHandler(command);
    }

    private void executeCommandWithErrorHandler(String command) {
        CommandController.executeCommand(command, this);
    }

    public List<Command> getCommands(){
        List<Command> commandList = new ArrayList<>();
        commandList.add(new Command("goTo", "rosservice call /uav" + GlobalSettings.UAV_NUMBER + "/trackers_manager/mpc_tracker/goTo "));
        commandList.add(new Command("goToAltitude", "rosservice call /uav" + GlobalSettings.UAV_NUMBER + "/trackers_manager/mpc_tracker/goToAltitude "));
        commandList.add(new Command("goToRelative", "rosservice call /uav" + GlobalSettings.UAV_NUMBER + "/trackers_manager/mpc_tracker/goToRelative "));

        return commandList;
    }

    @Override
    public void setResult(String result) {
        if(result == null){
            view.setErrorConnection(DronApplication.getsContext().getString(R.string.connection_error));
        }
    }
}
