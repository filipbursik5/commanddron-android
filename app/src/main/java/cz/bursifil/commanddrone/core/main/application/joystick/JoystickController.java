package cz.bursifil.commanddrone.core.main.application.joystick;

import cz.bursifil.commanddrone.core.PresenterCallback;

/**
 * Created by filas on 3/23/2018.
 */

public interface JoystickController {
    void executeJoystickCommand(String command, PresenterCallback presenterCallback);
}
