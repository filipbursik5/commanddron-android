package cz.bursifil.commanddrone.core.main.presentation.joystick;

import cz.bursifil.commanddrone.core.main.domain.MainStrings;

/**
 * Created by filas on 3/6/2018.
 */

public interface JoystickView {
    void setupData(MainStrings mainStrings);
    void commandError(String text);
}
