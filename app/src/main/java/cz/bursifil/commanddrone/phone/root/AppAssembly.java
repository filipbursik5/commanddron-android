package cz.bursifil.commanddrone.phone.root;

import cz.bursifil.commanddrone.phone.customcamera.injection.CustomCameraAssembly;
import cz.bursifil.commanddrone.phone.customcamera.view.CustomCameraActivity;
import cz.bursifil.commanddrone.phone.info.injection.InfoAssembly;
import cz.bursifil.commanddrone.phone.info.view.InfoActivity;
import cz.bursifil.commanddrone.phone.main.injection.MainAssembly;
import cz.bursifil.commanddrone.phone.main.view.MainActivity;
import cz.bursifil.commanddrone.phone.main.view.Tabs.CommandFragment;
import cz.bursifil.commanddrone.phone.main.view.Tabs.ConsoleFragment;
import cz.bursifil.commanddrone.phone.main.view.Tabs.FollowFragment;
import cz.bursifil.commanddrone.phone.main.view.Tabs.JoystickFragment;
import cz.bursifil.commanddrone.phone.saveddevices.injection.SavedDevicesAssembly;
import cz.bursifil.commanddrone.phone.saveddevices.view.SavedDevicesActivity;
import cz.bursifil.commanddrone.phone.settings.injection.SettingsAssembly;
import cz.bursifil.commanddrone.phone.settings.view.SettingsActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by filas on 3/6/2018.
 */

@Module
public abstract class AppAssembly {

    @ContributesAndroidInjector(modules = MainAssembly.class)
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector(modules = MainAssembly.class)
    abstract CommandFragment bindCommandFragment();

    @ContributesAndroidInjector(modules = MainAssembly.class)
    abstract ConsoleFragment bindConsoleFragment();

    @ContributesAndroidInjector(modules = MainAssembly.class)
    abstract FollowFragment bindFollowFragment();

    @ContributesAndroidInjector(modules = MainAssembly.class)
    abstract JoystickFragment bindJoystickFragment();

    @ContributesAndroidInjector(modules = InfoAssembly.class)
    abstract InfoActivity bindInfoActivity();

    @ContributesAndroidInjector(modules = SettingsAssembly.class)
    abstract SettingsActivity bindSettingsActivity();

    @ContributesAndroidInjector(modules = SavedDevicesAssembly.class)
    abstract SavedDevicesActivity bindSavedDevicesActivity();

    @ContributesAndroidInjector(modules = CustomCameraAssembly.class)
    abstract CustomCameraActivity bindCustomCameraActivity();
}
