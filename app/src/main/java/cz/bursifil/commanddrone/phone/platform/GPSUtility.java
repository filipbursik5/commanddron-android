package cz.bursifil.commanddrone.phone.platform;

import android.location.Location;

import cz.bursifil.commanddrone.core.main.domain.UTM;

/**
 * Created by filipbursik on 15.04.18.
 */

public interface GPSUtility {
    Location getLocation();
    Location moveGps(double brng, double dist, Location myLocation);
    UTM Deg2UTM(Location location);
}
