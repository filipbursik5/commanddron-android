package cz.bursifil.commanddrone.phone.platform;

/**
 * Created by filas on 3/23/2018.
 */

public interface AlertService {
    void showAlert(String text);
}
