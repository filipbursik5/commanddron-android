package cz.bursifil.commanddrone.phone.settings.view;

/**
 * Created by filas on 3/6/2018.
 */

public interface SettingsCallback {
    void saveCreditionals(String username, String hostname, String password);
}
