package cz.bursifil.commanddrone.phone.customcamera.view;

import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.customcamera.presentation.CustomCameraPresenter;
import cz.bursifil.commanddrone.core.customcamera.presentation.CustomCameraView;
import dagger.android.DaggerActivity;

/**
 * Created by filas on 3/18/2018.
 */

public class CustomCameraActivity extends DaggerActivity implements CustomCameraView {

    private SurfaceView preview = null;
    private SurfaceHolder previewHolder = null;
    private Camera camera = null;
    private boolean inPreview = false;
    ImageView image;

    private static final int MAX_VALUE_SEEKBAR = 30;

    @BindView(R.id.seekbar)
    SeekBar seekbar;
    @BindView(R.id.txt_seekbar_value)
    TextView txtValue;
    @BindView(R.id.txt_alpha)
    TextView alphaText;
    @BindView(R.id.txt_beta)
    TextView betaText;
    @BindView(R.id.txt_x)
    TextView xText;
    @BindView(R.id.txt_y)
    TextView yText;
    @BindView(R.id.txt_z)
    TextView zText;

    Unbinder unbinder;

    @Inject
    CustomCameraPresenter customCameraPresenter;

    SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                camera.setPreviewDisplay(previewHolder);
            } catch (Throwable t) {
                Log.e("PreviewDemo",
                        "Exception in setPreviewDisplay()", t);
                Toast.makeText(CustomCameraActivity.this, t.getMessage(), Toast.LENGTH_LONG)
                        .show();
            }
        }

        public void surfaceChanged(SurfaceHolder holder,
                                   int format, int width,
                                   int height) {
            Camera.Parameters parameters = camera.getParameters();
            Camera.Size size = getBestPreviewSize(width, height,
                    parameters);

            if (size != null) {
                parameters.setPreviewSize(size.width, size.height);
                camera.setParameters(parameters);
                camera.startPreview();
                inPreview = true;
            }
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            // no-op
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_camera);

        unbinder = ButterKnife.bind(this);

        image = (ImageView) findViewById(R.id.image);
        preview = (SurfaceView) findViewById(R.id.surface);

        previewHolder = preview.getHolder();
        previewHolder.addCallback(surfaceCallback);
        previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        previewHolder.setFixedSize(getWindow().getWindowManager()
                .getDefaultDisplay().getWidth(), getWindow().getWindowManager()
                .getDefaultDisplay().getHeight());
    }


    @Override
    public void onResume() {
        super.onResume();
        camera = Camera.open();
    }

    @Override
    public void onPause() {
        if (inPreview) {
            camera.stopPreview();
        }

        camera.release();
        camera = null;
        inPreview = false;
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        setSeekBarWithText();

        super.onStart();
        final SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        final SensorEventListener mEventListener = new SensorEventListener() {
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }

            public void onSensorChanged(SensorEvent event) {
                switch (event.sensor.getType()) {
                    case Sensor.TYPE_ACCELEROMETER:
                        customCameraPresenter.setValuesAccelerometr(event.values);
                        break;
                    case Sensor.TYPE_MAGNETIC_FIELD:
                        customCameraPresenter.setValuesMagnetic(event.values);
                        break;
                    case Sensor.TYPE_ORIENTATION:
                        customCameraPresenter.setBearing((double) Math.round(event.values[0]));
                        break;
                }
            }
        };

        setListners(sensorManager, mEventListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();

        customCameraPresenter.stopFollow();
    }

    public void setListners(SensorManager sensorManager, SensorEventListener mEventListener) {
        sensorManager.registerListener(mEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(mEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(mEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void setSeekBarWithText() {
        seekbar.setMax(MAX_VALUE_SEEKBAR);
        seekbar.setProgress(10);

        txtValue.setText(new Integer(seekbar.getProgress()).toString());
        customCameraPresenter.setSeekBarProgress(seekbar.getProgress());

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                txtValue.setText(new Integer(i).toString());
                customCameraPresenter.setSeekBarProgress(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private Camera.Size getBestPreviewSize(int width, int height, Camera.Parameters parameters) {
        Camera.Size result = null;
        for (Camera.Size size: parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size;
                } else {
                    int resultArea = result.width * result.height;
                    int newArea = size.width * size.height;
                    if (newArea > resultArea) {
                        result = size;
                    }
                }
            }
        }
        return (result);
    }

    @Override
    public void setInfo(final String alpha, final String beta, final String x, final String y, final String z) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alphaText.setText(alpha);
                betaText.setText(beta);
                xText.setText(x);
                yText.setText(y);
                zText.setText(z);
            }
        });
    }

    @OnClick(R.id.btn_follow)
    public void follow(View view){
        view.setVisibility(View.GONE);
        customCameraPresenter.follow();
    }
}
