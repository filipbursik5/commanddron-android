package cz.bursifil.commanddrone.phone.settings.view;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.settings.domain.SettingsStrings;
import cz.bursifil.commanddrone.core.settings.presentation.SettingsPresenter;
import cz.bursifil.commanddrone.core.settings.presentation.SettingsView;
import cz.bursifil.commanddrone.phone.platform.AlertService;
import cz.bursifil.commanddrone.phone.saveddevices.view.SavedDevicesActivity;
import dagger.android.support.DaggerAppCompatActivity;

public class SettingsActivity extends DaggerAppCompatActivity implements SettingsView {

    @BindView(R.id.text_status)
    TextView textStatus;

    @Inject
    AlertService alertService;
    @Inject
    SettingsPresenter settingsPresenter;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        settingsPresenter.onCreate();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @OnClick(R.id.btn_connectbutton)
    public void connect() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        ft.addToBackStack(null);

        SshConnectFragmentDialog newFragment = SshConnectFragmentDialog.newInstance();
        newFragment.setSettingsCallback(new SettingsCallback() {
            @Override
            public void saveCreditionals(String username, String hostname, String password) {
                settingsPresenter.saveCreditionals(hostname, password, username);
            }
        });

        newFragment.show(ft, "dialog");
    }

    @OnClick(R.id.btn_endsessiton)
    public void disconnect() {
        settingsPresenter.disconnect();
    }

    @OnClick(R.id.btn_savedevice)
    public void save(){
        settingsPresenter.saveDevice();
    }

    @OnClick(R.id.btn_choosefromfile)
    public void savedDevices(){
        startActivity(new Intent(this, SavedDevicesActivity.class));
    }

    @Override
    public void setupData(SettingsStrings settingsStrings) {
        setTitle(settingsStrings.getNavBartitle());
    }

    @Override
    public void setConnected(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textStatus.setBackgroundColor(Color.GREEN);
                textStatus.setText(text);
            }
        });
    }

    @Override
    public void setDisconnected(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textStatus.setText(text);
                textStatus.setBackgroundColor(Color.RED);
            }
        });
    }

    @Override
    public void setErrorConnection(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alertService.showAlert(text);
            }
        });
    }
}
