package cz.bursifil.commanddrone.phone.platform;

import android.widget.Toast;

import cz.bursifil.commanddrone.phone.root.DronApplication;

/**
 * Created by filas on 3/23/2018.
 */

public class AlertServiceImpl implements AlertService {

    @Override
    public void showAlert(String text) {
        Toast.makeText(DronApplication.getsContext(), text, Toast.LENGTH_LONG).show();
    }
}
