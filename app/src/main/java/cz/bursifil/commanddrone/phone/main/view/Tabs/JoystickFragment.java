package cz.bursifil.commanddrone.phone.main.view.Tabs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.main.domain.MainStrings;
import cz.bursifil.commanddrone.core.main.presentation.joystick.JoystickPresenter;
import dagger.android.support.DaggerFragment;
import io.github.controlwear.virtual.joystick.android.JoystickView;

/**
 * Created by filas on 12/4/2017.
 */

public class JoystickFragment extends DaggerFragment implements cz.bursifil.commanddrone.core.main.presentation.joystick.JoystickView {

    @BindView(R.id.joystick_joystick)
    JoystickView joystickJoystick;

    Unbinder unbinder;

    @Inject
    JoystickPresenter joystickPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_joystick, container, false);

        unbinder = ButterKnife.bind(this, view);
        joystickPresenter.onCreate();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        joystickJoystick.setOnMoveListener(new JoystickView.OnMoveListener() {

            @Override
            public void onMove(int angle, int strength) {
                joystickPresenter.setJoystickChange(angle);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            if (joystickJoystick == null) return;

            joystickJoystick.resetButtonPosition();
        }
    }

    @Override
    public void setupData(MainStrings mainStrings) {

    }

    @Override
    public void commandError(final String text) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
