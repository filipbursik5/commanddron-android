package cz.bursifil.commanddrone.phone.platform;

import cz.bursifil.commanddrone.phone.root.DronApplication;

/**
 * Created by filas on 3/23/2018.
 */

public class StringServiceImpl implements StringService {

    public String getStringById(Integer id){
        return DronApplication.getsContext().getString(id);
    }
}
