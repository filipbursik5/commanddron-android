package cz.bursifil.commanddrone.phone.main.view.Tabs;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.method.ScrollingMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.main.domain.MainStrings;
import cz.bursifil.commanddrone.core.main.presentation.console.ConsolePresenter;
import cz.bursifil.commanddrone.core.main.presentation.console.ConsoleView;
import dagger.android.support.DaggerFragment;

/**
 * Created by filas on 12/4/2017.
 */

public class ConsoleFragment extends DaggerFragment implements ConsoleView {

    @BindView(R.id.editText_command)
    EditText editTextCommand;
    @BindView(R.id.txt_console)
    TextView textConsole;

    Unbinder unbinder;

    @Inject
    ConsolePresenter consolePresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_console, container, false);

        unbinder = ButterKnife.bind(this, view);

        textConsole.setMovementMethod(new ScrollingMovementMethod());


        editTextCommand.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    textConsole.append(consolePresenter.getFormattedCommand(v.getEditableText()));
                    consolePresenter.perform(v.getEditableText());
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind( );
    }

    @Override
    public void setupData(MainStrings mainStrings) {

    }

    @Override
    public void addToConsole(final String text) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int start = textConsole.getText().length();
                textConsole.append(text);
                int end = textConsole.getText().length();

                Spannable spannableText = (Spannable) textConsole.getText();
                spannableText.setSpan(new ForegroundColorSpan(Color.GREEN), start, end, 0);
            }
        });
    }

    @Override
    public void addErrorToConsole(final String string) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int start = textConsole.getText().length();
                textConsole.append(string);
                int end = textConsole.getText().length();

                Spannable spannableText = (Spannable) textConsole.getText();
                spannableText.setSpan(new ForegroundColorSpan(Color.RED), start, end, 0);
            }
        });
    }
}