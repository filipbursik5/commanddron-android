package cz.bursifil.commanddrone.phone.saveddevices.view;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.saveddevices.domain.Device;
import cz.bursifil.commanddrone.core.saveddevices.presentation.SavedDevicesPresenter;

/**
 * Created by filas on 8/28/2017.
 */

public class SavedDevicesAdapter extends RecyclerView.Adapter<SavedDevicesAdapter.ViewHolder> {

    private Activity activity;
    private List<Device> deviceList;
    private SavedDevicesPresenter savedDevicesPresenter;

    public SavedDevicesAdapter(Activity activity,
                               List<Device> deviceList, SavedDevicesPresenter savedDevicesPresenter) {
        this.activity = activity;
        this.deviceList = deviceList;
        this.savedDevicesPresenter = savedDevicesPresenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_saved_devices, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.name.setText(deviceList.get(position).getUsername());
    }

    @Override
    public int getItemCount() {
        return deviceList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startCommand();
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    deleteDevice();

                    return false;
                }
            });
        }

        private void startCommand(){
            Device device = deviceList.get(getAdapterPosition());
            savedDevicesPresenter.startConnecting(device);
        }

        private void deleteDevice(){
            int position = getAdapterPosition();
            savedDevicesPresenter.deleteDevice(position);
            deviceList.remove(position);

            notifyDataSetChanged();
        }
    }
}