package cz.bursifil.commanddrone.phone.customcamera.injection;

import cz.bursifil.commanddrone.core.customcamera.application.CustomCameraController;
import cz.bursifil.commanddrone.core.customcamera.presentation.CustomCameraPresenter;
import cz.bursifil.commanddrone.core.customcamera.presentation.CustomCameraPresenterImpl;
import cz.bursifil.commanddrone.core.customcamera.presentation.CustomCameraView;
import cz.bursifil.commanddrone.phone.customcamera.view.CustomCameraActivity;
import cz.bursifil.commanddrone.phone.platform.GPSUtility;
import cz.bursifil.commanddrone.phone.platform.GPSUtilityImpl;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by filas on 3/6/2018.
 */
@Module
public abstract class CustomCameraAssembly {

    @Provides
    static CustomCameraPresenter provideCustomCameraPresenter(CustomCameraView customCameraView, CustomCameraController customCameraController, GPSUtility gpsUtility) {
        return new CustomCameraPresenterImpl(customCameraView, customCameraController, gpsUtility);
    }

    @Binds
    abstract CustomCameraView provideCustomCameraView(CustomCameraActivity customCameraActivity);

    @Provides
    static CustomCameraController provideCustomCameraController() {
        return new CustomCameraController();
    }

    @Provides
    static GPSUtility provideGPSUtility() {
        return new GPSUtilityImpl();
    }
}
