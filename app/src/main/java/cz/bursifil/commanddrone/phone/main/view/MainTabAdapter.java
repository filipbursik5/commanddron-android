package cz.bursifil.commanddrone.phone.main.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import cz.bursifil.commanddrone.phone.main.view.Tabs.CommandFragment;
import cz.bursifil.commanddrone.phone.main.view.Tabs.ConsoleFragment;
import cz.bursifil.commanddrone.phone.main.view.Tabs.FollowFragment;
import cz.bursifil.commanddrone.phone.main.view.Tabs.JoystickFragment;

/**
 * Created by Filip on 21.09.2017.
 */

public class MainTabAdapter extends FragmentPagerAdapter {

    public MainTabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new CommandFragment();
            case 1:
                return new ConsoleFragment();
            case 2:
                return new FollowFragment();
            case 3:
                return new JoystickFragment();
            default:
                return new Fragment();
        }
    }

    @Override
    public int getCount() {
        return 4;
    }
}
