package cz.bursifil.commanddrone.phone.main.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.main.domain.MainStrings;
import cz.bursifil.commanddrone.core.main.presentation.MainPresenter;
import cz.bursifil.commanddrone.core.main.presentation.MainView;
import cz.bursifil.commanddrone.phone.info.view.InfoActivity;
import cz.bursifil.commanddrone.phone.platform.AlertService;
import cz.bursifil.commanddrone.phone.platform.StringService;
import cz.bursifil.commanddrone.phone.settings.view.SettingsActivity;
import cz.cvut.fel.androidsshcommunication.sshutils.SessionController;
import dagger.android.support.DaggerAppCompatActivity;

public class MainActivity extends DaggerAppCompatActivity implements MainView {

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.text_status)
    TextView textStatus;

    @Inject
    MainPresenter mainPresenter;
    @Inject
    AlertService alertService;
    @Inject
    StringService stringService;


    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        askForPermissions();

        mainPresenter.onCreate();
    }

    @Override
    public void onResume(){
        super.onResume();

        mainPresenter.onResume();
    }

    @Override
    public void setupData(MainStrings mainStrings) {
        setTitle(mainStrings.getNavBartitle());
        setupPagerAndTab(mainStrings);
    }

    @Override
    public void deviceConnected(String text) {
        textStatus.setText(text);
        textStatus.setBackgroundColor(Color.GREEN);
    }
    
    @Override
    public void deviceDisconnected(String text) {
        textStatus.setText(text);
        textStatus.setBackgroundColor(Color.RED);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.action_info:
                if(!SessionController.isConnected()) {
                    alertService.showAlert(stringService.getStringById(R.string.connection_error));
                    return true;
                }

                startActivity(new Intent(this, InfoActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupPagerAndTab(MainStrings mainStrings) {

        tabLayout.removeAllTabs();

        tabLayout.addTab(tabLayout.newTab().setText(mainStrings.getCommandsTitle()));
        tabLayout.addTab(tabLayout.newTab().setText(mainStrings.getConsoleTitle()));
        tabLayout.addTab(tabLayout.newTab().setText(mainStrings.getFollowTitle()));
        tabLayout.addTab(tabLayout.newTab().setText(mainStrings.getJoystickTitle()));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }

        });

        final MainTabAdapter adapter = new MainTabAdapter(getSupportFragmentManager());
        viewpager.setAdapter(adapter);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    private void askForPermissions(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    0);
        }
    }
}
