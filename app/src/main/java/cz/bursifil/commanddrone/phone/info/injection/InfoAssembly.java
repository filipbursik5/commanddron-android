package cz.bursifil.commanddrone.phone.info.injection;

import cz.bursifil.commanddrone.core.info.application.InfoController;
import cz.bursifil.commanddrone.core.info.application.InfoControllerImpl;
import cz.bursifil.commanddrone.core.info.presentation.InfoPresenter;
import cz.bursifil.commanddrone.core.info.presentation.InfoPresenterImpl;
import cz.bursifil.commanddrone.core.info.presentation.InfoView;
import cz.bursifil.commanddrone.phone.info.view.InfoActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by filas on 3/6/2018.
 */
@Module
public abstract class InfoAssembly {

    @Provides
    static InfoPresenter provideInfoPresenter(InfoView infoView, InfoController infoController) {
        return new InfoPresenterImpl(infoView, infoController);
    }

    @Binds
    abstract InfoView provideInfoView(InfoActivity infoActivity);

    @Provides
    static InfoController provideInfoController() {
        return new InfoControllerImpl();
    }
}
