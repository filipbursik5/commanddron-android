package cz.bursifil.commanddrone.phone.settings.view;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import cz.bursifil.commanddrone.R;

public class SshConnectFragmentDialog  extends DialogFragment implements View.OnClickListener {

    private EditText mUserEdit;
    private EditText mHostEdit;
    private EditText mPasswordEdit;
    private EditText mPortNumEdit;
    private Button mButton;

    private SettingsCallback settingsCallback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setSettingsCallback(SettingsCallback settingsCallback) {
        this.settingsCallback = settingsCallback;
    }
    public static SshConnectFragmentDialog newInstance() {
        SshConnectFragmentDialog fragment = new SshConnectFragmentDialog();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_connect, container, false);
        mUserEdit = (EditText) v.findViewById(R.id.username);
        mHostEdit = (EditText) v.findViewById(R.id.hostname);
        mPasswordEdit = (EditText) v.findViewById(R.id.password);
        mPortNumEdit = (EditText) v.findViewById(R.id.portnum);
        mButton = (Button) v.findViewById(R.id.enterbutton);
        mButton.setOnClickListener(this);
        return v;
    }

    private boolean isEditTextEmpty(EditText editText) {
        if (editText.getText() == null || editText.getText().toString().equalsIgnoreCase("")) {
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v == mButton) {
            if (isEditTextEmpty(mUserEdit) || isEditTextEmpty(mHostEdit)
                    || isEditTextEmpty(mPasswordEdit) || isEditTextEmpty(mPortNumEdit)) {
                return;
            }

            settingsCallback.saveCreditionals(mUserEdit.getText().toString(), mHostEdit.getText().toString(),
                    mPasswordEdit.getText().toString());

            this.dismiss();
        }
    }
}


