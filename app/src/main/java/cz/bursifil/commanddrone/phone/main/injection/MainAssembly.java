package cz.bursifil.commanddrone.phone.main.injection;

import cz.bursifil.commanddrone.core.customcamera.application.CustomCameraController;
import cz.bursifil.commanddrone.core.main.application.joystick.JoystickController;
import cz.bursifil.commanddrone.core.main.presentation.command.CommandPresenter;
import cz.bursifil.commanddrone.core.main.presentation.command.CommandPresenterImpl;
import cz.bursifil.commanddrone.core.main.presentation.command.CommandView;
import cz.bursifil.commanddrone.core.main.application.console.ConsoleController;
import cz.bursifil.commanddrone.core.main.presentation.console.ConsolePresenter;
import cz.bursifil.commanddrone.core.main.presentation.console.ConsolePresenterImpl;
import cz.bursifil.commanddrone.core.main.presentation.console.ConsoleView;
import cz.bursifil.commanddrone.core.main.application.follow.FollowController;
import cz.bursifil.commanddrone.core.main.presentation.follow.FollowPresenter;
import cz.bursifil.commanddrone.core.main.presentation.follow.FollowPresenterImpl;
import cz.bursifil.commanddrone.core.main.presentation.follow.FollowView;
import cz.bursifil.commanddrone.core.main.application.joystick.JoystickControllerImpl;
import cz.bursifil.commanddrone.core.main.presentation.joystick.JoystickPresenter;
import cz.bursifil.commanddrone.core.main.presentation.joystick.JoystickPresenterImpl;
import cz.bursifil.commanddrone.core.main.presentation.joystick.JoystickView;
import cz.bursifil.commanddrone.core.main.presentation.MainPresenter;
import cz.bursifil.commanddrone.core.main.presentation.MainPresenterImpl;
import cz.bursifil.commanddrone.core.main.presentation.MainView;
import cz.bursifil.commanddrone.phone.main.view.MainActivity;
import cz.bursifil.commanddrone.phone.main.view.Tabs.CommandFragment;
import cz.bursifil.commanddrone.phone.main.view.Tabs.ConsoleFragment;
import cz.bursifil.commanddrone.phone.main.view.Tabs.FollowFragment;
import cz.bursifil.commanddrone.phone.main.view.Tabs.JoystickFragment;
import cz.bursifil.commanddrone.phone.platform.AlertService;
import cz.bursifil.commanddrone.phone.platform.AlertServiceImpl;
import cz.bursifil.commanddrone.phone.platform.GPSUtility;
import cz.bursifil.commanddrone.phone.platform.GPSUtilityImpl;
import cz.bursifil.commanddrone.phone.platform.StringService;
import cz.bursifil.commanddrone.phone.platform.StringServiceImpl;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by filas on 3/6/2018.
 */
@Module
public abstract class MainAssembly {

    @Provides
    static MainPresenter provideMainPresenter(MainView mainView, StringService stringServiceImpl) {
        return new MainPresenterImpl(mainView, stringServiceImpl);
    }

    @Provides
    static ConsolePresenter provideConsolePresenter(ConsoleView consoleView, ConsoleController consoleController) {
        return new ConsolePresenterImpl(consoleView, consoleController);
    }

    @Provides
    static CommandPresenter provideCommandPresenter(CommandView commandView) {
        return new CommandPresenterImpl(commandView);
    }

    @Provides
    static FollowPresenter provideFollowPresenter(FollowView followView, FollowController followController, GPSUtility gpsUtility) {
        return new FollowPresenterImpl(followView, followController, gpsUtility);
    }

    @Provides
    static JoystickPresenter provideJoystickPresenter(JoystickView joystickView, JoystickController joystickController) {
        return new JoystickPresenterImpl(joystickView, joystickController);
    }

    @Binds
    abstract MainView provideMainView(MainActivity mainActivity);

    @Binds
    abstract ConsoleView provideConsoleView(ConsoleFragment consoleFragment);

    @Binds
    abstract CommandView provideCommandView(CommandFragment commandFragment);

    @Binds
    abstract FollowView provideFollowView(FollowFragment followFragment);

    @Binds
    abstract JoystickView provideJoystickView(JoystickFragment joystickFragment);

    @Provides
    static ConsoleController provideConsoleController() {
        return new ConsoleController();
    }

    @Provides
    static FollowController provideFollowController() {
        return new FollowController();
    }

    @Provides
    static JoystickController provideJoystickController() {
        return new JoystickControllerImpl();
    }

    @Provides
    static StringService provideStringService() {
        return new StringServiceImpl();
    }

    @Provides
    static AlertService provideAlertService() {
        return new AlertServiceImpl();
    }

    @Provides
    static GPSUtility provideGPSUtility() {
        return new GPSUtilityImpl();
    }

    @Provides
    static CustomCameraController provideCustomCameraController() {
        return new CustomCameraController();
    }
}
