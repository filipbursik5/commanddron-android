package cz.bursifil.commanddrone.phone.root;

import android.content.Context;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

/**
 * Created by filas on 3/6/2018.
 */

public class DronApplication extends DaggerApplication {

    private static Context sContext;

    protected static AppComponent sMainComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        sContext = getApplicationContext();
    }

    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder().application(this).build();
        appComponent.inject(this);
        return appComponent;
    }

    public static Context getsContext() {
        return sContext;
    }
}
