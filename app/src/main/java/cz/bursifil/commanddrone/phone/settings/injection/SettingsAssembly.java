package cz.bursifil.commanddrone.phone.settings.injection;

import cz.bursifil.commanddrone.core.settings.application.SettingsController;
import cz.bursifil.commanddrone.core.settings.application.SettingsControllerImpl;
import cz.bursifil.commanddrone.core.settings.presentation.SettingsPresenter;
import cz.bursifil.commanddrone.core.settings.presentation.SettingsPresenterImpl;
import cz.bursifil.commanddrone.core.settings.presentation.SettingsView;
import cz.bursifil.commanddrone.phone.platform.AlertService;
import cz.bursifil.commanddrone.phone.platform.AlertServiceImpl;
import cz.bursifil.commanddrone.phone.settings.view.SettingsActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by filas on 3/6/2018.
 */
@Module
public abstract class SettingsAssembly {

    @Provides
    static SettingsPresenter provideSettingsPresenter(SettingsView infoView, SettingsController settingsController) {
        return new SettingsPresenterImpl(infoView, settingsController);
    }

    @Binds
    abstract SettingsView provideSettingsView(SettingsActivity settingsActivity);

    @Provides
    static SettingsController provideSettingsController() {
        return new SettingsControllerImpl();
    }

    @Provides
    static AlertService provideAlertService() {
        return new AlertServiceImpl();
    }
}
