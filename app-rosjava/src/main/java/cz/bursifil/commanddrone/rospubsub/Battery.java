package cz.bursifil.commanddrone.rospubsub;

/**
 * Created by filipbursik on 15.04.18.
 */

public class Battery {

    private String voltage;
    private String percetange;

    public Battery(String voltage, String percetange) {
        this.voltage = voltage;
        this.percetange = percetange;
    }

    public String getVoltage() {
        return voltage;
    }

    public String getPercetange() {
        return percetange;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public void setPercetange(String percetange) {
        this.percetange = percetange;
    }
}
