/*
 * Copyright (C) 2014 Oliver Degener.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package cz.bursifil.commanddrone.rospubsub;

import android.util.Log;

import com.example.globalsettings.GlobalSettings;

import org.ros.concurrent.CancellableLoop;
import org.ros.message.MessageListener;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.NodeMain;
import org.ros.node.topic.Subscriber;

import sensor_msgs.BatteryState;
import std_msgs.String;

public class SimpleSubscriberNode extends AbstractNodeMain implements NodeMain {

    private BatteryCallback batteryCallback;

    public SimpleSubscriberNode(BatteryCallback batteryCallback) {
        this.batteryCallback = batteryCallback;
    }

    @Override
    public GraphName getDefaultNodeName() {
        return GraphName.of("");
    }

    @Override
    public void onStart(ConnectedNode connectedNode) {
        final Subscriber<BatteryState> subscriber = connectedNode.newSubscriber("uav" + GlobalSettings.UAV_NUMBER + "/mavros/battery", BatteryState._TYPE);

        subscriber.addMessageListener(new MessageListener<BatteryState>() {
            @Override
            public void onNewMessage(BatteryState batteryState) {
                batteryCallback.returnBattery(new Battery(new Float(batteryState.getVoltage()).toString(), new Float(batteryState.getPercentage()).toString()));
            }
        });
    }
}
