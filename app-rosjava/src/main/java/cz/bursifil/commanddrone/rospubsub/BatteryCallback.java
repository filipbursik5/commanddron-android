package cz.bursifil.commanddrone.rospubsub;

/**
 * Created by filipbursik on 15.04.18.
 */

public interface BatteryCallback {
    void returnBattery(Battery battery);
}
