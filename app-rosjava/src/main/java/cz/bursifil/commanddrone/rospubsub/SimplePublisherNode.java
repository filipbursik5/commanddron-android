/*
 * Copyright (C) 2014 Oliver Degener.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package cz.bursifil.commanddrone.rospubsub;

import android.location.Location;

import org.ros.concurrent.CancellableLoop;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.NodeMain;
import org.ros.node.topic.Publisher;

public class SimplePublisherNode extends AbstractNodeMain implements NodeMain {

    public String x;
    public String y;
    public String z;
    public String x_vector;
    public String y_vector;
    public String z_vector;
    public Location myLocation;

    private CancellableLoop cancellableLoop;

    @Override
    public GraphName getDefaultNodeName() {
        return GraphName.of("SimplePublisher/TimeLoopNode");
    }

    @Override
    public void onStart(final ConnectedNode connectedNode) {
        final Publisher<std_msgs.String> publisher = connectedNode.newPublisher(GraphName.of("uav/set_position"), std_msgs.String._TYPE);

        cancellableLoop = new CancellableLoop() {
            @Override
            protected void loop() throws InterruptedException {
                std_msgs.String str = publisher.newMessage();
                str.setData(x + " " + y + " " + z + " " + x_vector + " " + y_vector
                        + " " + z_vector + " " + myLocation.getLatitude() + " " + myLocation.getLongitude());
                publisher.publish(str);

                Thread.sleep(300);
            }
        };
        connectedNode.executeCancellableLoop(cancellableLoop);
    }

    public void stop(){
        cancellableLoop.cancel();
    }
}
