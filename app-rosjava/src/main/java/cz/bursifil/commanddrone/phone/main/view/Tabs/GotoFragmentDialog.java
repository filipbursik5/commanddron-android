package cz.bursifil.commanddrone.phone.main.view.Tabs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.phone.platform.StringService;


public class GotoFragmentDialog extends android.app.DialogFragment implements View.OnClickListener {

    private EditText x;
    private EditText y;
    private EditText z;
    private EditText tilt;
    private Button mButton;

    private GoToListener mListener;
    private StringService stringService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setStringService(StringService stringService) {
        this.stringService = stringService;
    }

    public void setListener(GoToListener listenr){
        mListener = listenr;
    }

    public static GotoFragmentDialog newInstance() {
        GotoFragmentDialog fragment = new GotoFragmentDialog();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_goto, container, false);
        x = (EditText) v.findViewById(R.id.x);
        y = (EditText) v.findViewById(R.id.y);
        z = (EditText) v.findViewById(R.id.z);
        tilt = (EditText) v.findViewById(R.id.tilt);
        mButton = (Button) v.findViewById(R.id.okbutton);
        mButton.setOnClickListener(this);
        return v;
    }

    private boolean isEditTextEmpty(EditText editText) {
        if (editText.getText() == null || editText.getText().toString().equalsIgnoreCase("")) {
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v == mButton) {
            if (isEditTextEmpty(x)) {
                x.setError(stringService.getStringById(R.string.have_to_be_filled));
                return;
            }

            if (isEditTextEmpty(y) ){
                y.setError(stringService.getStringById(R.string.have_to_be_filled));
                return;
            }

            if (isEditTextEmpty(z)) {
                z.setError(stringService.getStringById(R.string.have_to_be_filled));
                return;
            }

            if (isEditTextEmpty(tilt)) {
                tilt.setError(stringService.getStringById(R.string.have_to_be_filled));
                return;
            }

            mListener.goTo(x.getText().toString(), y.getText().toString()
                    ,z.getText().toString(), tilt.getText().toString());
        }
    }
}


