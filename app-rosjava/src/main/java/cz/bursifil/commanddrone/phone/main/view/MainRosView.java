package cz.bursifil.commanddrone.phone.main.view;

import android.content.Intent;
import android.os.Bundle;

import org.ros.address.InetAddressFactory;
import org.ros.android.RosActivity;
import org.ros.node.NodeConfiguration;
import org.ros.node.NodeMainExecutor;

/**
 * Created by filas on 4/14/2018.
 */

public class MainRosView extends RosActivity {

    public static NodeMainExecutor nodeMainExecutor;
    public static NodeConfiguration nodeConfiguration;

    public MainRosView() {
        super("RosAndroidExample", "RosAndroidExample");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void init(NodeMainExecutor nodeMainExecutor) {
        NodeConfiguration nodeConfiguration = NodeConfiguration.newPublic(InetAddressFactory.newNonLoopback().getHostAddress());
        nodeConfiguration.setMasterUri(getMasterUri());

        MainRosView.nodeConfiguration = nodeConfiguration;
        MainRosView.nodeMainExecutor = nodeMainExecutor;

        startActivity(new Intent(this, MainActivity.class));
    }
}
