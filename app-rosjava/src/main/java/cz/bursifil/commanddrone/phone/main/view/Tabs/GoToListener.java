package cz.bursifil.commanddrone.phone.main.view.Tabs;

/**
 * Created by filas on 3/6/2018.
 */

public interface GoToListener {
    void goTo(String x, String y, String z, String tilt);
    void goToAltitude(String altitude);
}
