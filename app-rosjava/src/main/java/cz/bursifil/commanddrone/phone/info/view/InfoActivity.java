package cz.bursifil.commanddrone.phone.info.view;

import android.os.Bundle;
import android.widget.TextView;

import javax.inject.Inject;

import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.info.domain.InfoStrings;
import cz.bursifil.commanddrone.core.info.presentation.InfoPresenter;
import cz.bursifil.commanddrone.core.info.presentation.InfoView;
import dagger.android.support.DaggerAppCompatActivity;

public class InfoActivity extends DaggerAppCompatActivity implements InfoView {

    TextView txtBattery;
    TextView txtBatteryVoltage;

    @Inject
    InfoPresenter infoPresenter;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_info);

        txtBattery = (TextView) findViewById(R.id.txt_battery);
        txtBatteryVoltage = (TextView) findViewById(R.id.txt_battery_voltage);
    }

    @Override
    public void onStart(){
        super.onStart();

        infoPresenter.onCreate();
    }

    @Override
    public void setupData(InfoStrings infoStrings) {
        setTitle(infoStrings.getNavBartitle());
    }

    @Override
    public void showBattery(final String battery) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                txtBattery.setText(battery);
            }
        });
    }

    @Override
    public void showVoltage(final String voltage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                txtBatteryVoltage.setText(voltage);
            }
        });
    }
}