package cz.bursifil.commanddrone.phone.main.view.Tabs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.main.domain.Command;
import cz.bursifil.commanddrone.core.main.domain.CommandStrings;
import cz.bursifil.commanddrone.core.main.presentation.command.CommandPresenter;
import cz.bursifil.commanddrone.core.main.presentation.command.CommandView;
import cz.bursifil.commanddrone.phone.platform.AlertService;
import cz.bursifil.commanddrone.phone.platform.StringService;
import dagger.android.support.DaggerFragment;

/**
 * Created by filas on 12/4/2017.
 */

public class CommandFragment extends DaggerFragment implements CommandView {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    Unbinder unbinder;

    @Inject
    AlertService alertService;
    @Inject
    StringService stringService;
    @Inject
    CommandPresenter commandPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_commands, container, false);

        unbinder = ButterKnife.bind(this, view);
        commandPresenter.onCreate();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setupData(CommandStrings commandStrings) {

    }

    @Override
    public void showCommands(List<Command> commands) {
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        CommandsAdapter commandsAdapter = new CommandsAdapter(getActivity(), commands, commandPresenter, stringService);
        recyclerView.setAdapter(commandsAdapter);
        recyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void setErrorConnection(final String text) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alertService.showAlert(text);
            }
        });
    }
}
