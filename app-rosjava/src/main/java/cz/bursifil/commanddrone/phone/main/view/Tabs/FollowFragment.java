package cz.bursifil.commanddrone.phone.main.view.Tabs;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.AppCompatSeekBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.main.domain.MainStrings;
import cz.bursifil.commanddrone.core.main.presentation.follow.FollowPresenter;
import cz.bursifil.commanddrone.core.main.presentation.follow.FollowView;
import cz.bursifil.commanddrone.phone.customcamera.view.CustomCameraActivity;
import cz.bursifil.commanddrone.phone.platform.AlertService;
import cz.bursifil.commanddrone.phone.root.DronApplication;
import dagger.android.support.DaggerFragment;

import static android.content.Context.SENSOR_SERVICE;

/**
 * Created by filas on 12/4/2017.
 */

public class FollowFragment extends DaggerFragment implements FollowView, OnMapReadyCallback {

    private static final int MAX_VALUE_SEEKBAR = 150;

    @BindView(R.id.txt_value)
    TextView txtValue;
    @BindView(R.id.seekbar)
    AppCompatSeekBar seekbar;
    @BindView(R.id.following)
    Button following;
    @BindView(R.id.txt_alpha)
    TextView alphaText;
    @BindView(R.id.txt_beta)
    TextView betaText;
    @BindView(R.id.txt_x)
    TextView xText;
    @BindView(R.id.txt_y)
    TextView yText;
    @BindView(R.id.txt_z)
    TextView zText;
    @BindView(R.id.maps)
    MapView mMapView;

    @Inject
    FollowPresenter followPresenter;

    private GoogleMap mGoogleMap;
    Unbinder unbinder;
    private Marker marker;
    private boolean saving;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_follow, container, false);

        unbinder = ButterKnife.bind(this, view);
        followPresenter.onCreate();
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(FollowFragment.this);

        return view;
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mGoogleMap = googleMap;

        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(true);
        if (ActivityCompat.checkSelfPermission(DronApplication.getsContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(DronApplication.getsContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
           return;
        }
        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        googleMap.setMyLocationEnabled(true);
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onStart() {
        super.onStart();

        setSeekBarWithText();

        super.onStart();
        final SensorManager sensorManager = (SensorManager) getActivity().getSystemService(SENSOR_SERVICE);

        final SensorEventListener mEventListener = new SensorEventListener() {
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }

            public void onSensorChanged(SensorEvent event) {

                switch (event.sensor.getType()) {
                    case Sensor.TYPE_ACCELEROMETER:
                        followPresenter.setValuesAccelerometr(event.values);
                        break;
                    case Sensor.TYPE_MAGNETIC_FIELD:
                        followPresenter.setValuesMagnetic(event.values);
                        break;
                    case Sensor.TYPE_ORIENTATION:
                        followPresenter.setBearing((double) Math.round(event.values[0]));
                        break;
                }
            }
        };

        setListners(sensorManager, mEventListener);
    }

    public void setListners(SensorManager sensorManager, SensorEventListener mEventListener) {
        sensorManager.registerListener(mEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(mEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(mEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void setSeekBarWithText() {
        seekbar.setMax(MAX_VALUE_SEEKBAR);
        seekbar.setProgress(10);

        txtValue.setText(new Integer(seekbar.getProgress()).toString());
        followPresenter.setSeekBarProgress(seekbar.getProgress());

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                txtValue.setText(new Integer(i).toString());
                followPresenter.setSeekBarProgress(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setupData(MainStrings mainStrings) {

    }

    @Override
    public void setFollowing(final String text) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                following.setText(text);
            }
        });
    }

    @Override
    public void setNotFollowing(final String text) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                following.setText(text);
            }
        });
    }

    @Override
    public void followingError(final String text) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void setInfo(final String alpha, final String beta, final String x, final String y, final String z) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alphaText.setText(alpha);
                betaText.setText(beta);
                xText.setText(x);
                yText.setText(y);
                zText.setText(z);
            }
        });
    }

    @Override
    public void addMarker(Location location) {
        final LatLng position = new LatLng(location.getLatitude(), location.getLongitude());

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(marker != null){
                    marker.remove();
                }

                marker = mGoogleMap.addMarker(new MarkerOptions().position(position)
                        .title("Dron"));
            }
        });
    }

    @OnClick(R.id.following)
    public void follow(){
        followPresenter.follow();
    }

    @OnClick(R.id.btn_use_camera)
    public void startCamera(){
        startActivity(new Intent(getActivity(), CustomCameraActivity.class));
    }
}
