package cz.bursifil.commanddrone.phone.main.view.Tabs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.phone.platform.StringService;


public class AltitudeFragmentDialog extends android.app.DialogFragment implements View.OnClickListener {

    private EditText altitude;
    private Button mButton;

    private GoToListener mListener;
    private StringService stringService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setStringService(StringService stringService) {
        this.stringService = stringService;
    }

    public void setListener(GoToListener listenr){
        mListener = listenr;
    }

    public static AltitudeFragmentDialog newInstance() {
        AltitudeFragmentDialog fragment = new AltitudeFragmentDialog();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_altitude, container, false);
        altitude = (EditText) v.findViewById(R.id.altitude);
        mButton = (Button) v.findViewById(R.id.okbutton);
        mButton.setOnClickListener(this);
        return v;
    }

    private boolean isEditTextEmpty(EditText editText) {
        if (editText.getText() == null || editText.getText().toString().equalsIgnoreCase("")) {
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v == mButton) {
            if (isEditTextEmpty(altitude)) {
                altitude.setError(stringService.getStringById(R.string.have_to_be_filled));
                return;
            }

            mListener.goToAltitude(altitude.getText().toString());
        }
    }
}


