package cz.bursifil.commanddrone.phone.saveddevices.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.main.domain.MainStrings;
import cz.bursifil.commanddrone.core.saveddevices.domain.Device;
import cz.bursifil.commanddrone.core.saveddevices.presentation.SavedDevicesPresenter;
import cz.bursifil.commanddrone.core.saveddevices.presentation.SavedDevicesView;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by filipbursik on 06.12.17.
 */

public class SavedDevicesActivity extends DaggerAppCompatActivity implements SavedDevicesView {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @Inject
    SavedDevicesPresenter savedDevicesPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_saved_devices);
        ButterKnife.bind(this);

        savedDevicesPresenter.onCreate();

        setTitle(R.string.saveddevices);
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void setupData(MainStrings mainStrings) {

    }

    @Override
    public void showDevices(List<Device> devices) {
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        SavedDevicesAdapter keeperAdapter = new SavedDevicesAdapter(this, devices, savedDevicesPresenter);
        recyclerView.setAdapter(keeperAdapter);
        recyclerView.setLayoutManager(mLayoutManager);
    }
}
