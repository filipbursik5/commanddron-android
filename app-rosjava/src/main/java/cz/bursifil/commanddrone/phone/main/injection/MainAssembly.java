package cz.bursifil.commanddrone.phone.main.injection;

import cz.bursifil.commanddrone.core.main.application.follow.FollowController;
import cz.bursifil.commanddrone.core.main.presentation.MainPresenter;
import cz.bursifil.commanddrone.core.main.presentation.MainPresenterImpl;
import cz.bursifil.commanddrone.core.main.presentation.MainView;
import cz.bursifil.commanddrone.core.main.presentation.command.CommandPresenter;
import cz.bursifil.commanddrone.core.main.presentation.command.CommandPresenterImpl;
import cz.bursifil.commanddrone.core.main.presentation.command.CommandView;
import cz.bursifil.commanddrone.core.main.presentation.follow.FollowPresenter;
import cz.bursifil.commanddrone.core.main.presentation.follow.FollowPresenterImpl;
import cz.bursifil.commanddrone.core.main.presentation.follow.FollowView;
import cz.bursifil.commanddrone.phone.main.view.MainActivity;
import cz.bursifil.commanddrone.phone.main.view.Tabs.CommandFragment;
import cz.bursifil.commanddrone.phone.main.view.Tabs.FollowFragment;
import cz.bursifil.commanddrone.phone.platform.AlertService;
import cz.bursifil.commanddrone.phone.platform.AlertServiceImpl;
import cz.bursifil.commanddrone.phone.platform.GPSUtility;
import cz.bursifil.commanddrone.phone.platform.GPSUtilityImpl;
import cz.bursifil.commanddrone.phone.platform.StringService;
import cz.bursifil.commanddrone.phone.platform.StringServiceImpl;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by filas on 3/6/2018.
 */
@Module
public abstract class MainAssembly {

    @Provides
    static MainPresenter provideMainPresenter(MainView mainView, StringService stringServiceImpl) {
        return new MainPresenterImpl(mainView, stringServiceImpl);
    }

    @Provides
    static CommandPresenter provideCommandPresenter(CommandView commandView) {
        return new CommandPresenterImpl(commandView);
    }

    @Provides
    static FollowPresenter provideFollowPresenter(FollowView followView, FollowController followController, GPSUtility gpsUtility) {
        return new FollowPresenterImpl(followView, followController, gpsUtility);
    }

    @Binds
    abstract MainView provideMainView(MainActivity mainActivity);

    @Binds
    abstract CommandView provideCommandView(CommandFragment commandFragment);

    @Binds
    abstract FollowView provideFollowView(FollowFragment followFragment);

    @Provides
    static FollowController provideFollowController() {
        return new FollowController();
    }

    @Provides
    static StringService provideStringService() {
        return new StringServiceImpl();
    }

    @Provides
    static AlertService provideAlertService() {
        return new AlertServiceImpl();
    }

    @Provides
    static GPSUtility provideGPSUtility() {
        return new GPSUtilityImpl();
    }
}
