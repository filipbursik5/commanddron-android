package cz.bursifil.commanddrone.phone.root;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by Filip on 3/6/2018.
 */

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class,
        AppAssembly.class})
public interface AppComponent extends AndroidInjector<DaggerApplication> {

    void inject(DronApplication app);

    @Override
    void inject(DaggerApplication instance);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        AppComponent build();
    }
}
