package cz.bursifil.commanddrone.phone.saveddevices.injection;

import cz.bursifil.commanddrone.core.saveddevices.application.SavedDevicesController;
import cz.bursifil.commanddrone.core.saveddevices.application.SavedDevicesControllerImpl;
import cz.bursifil.commanddrone.core.saveddevices.presentation.SavedDevicesPresenter;
import cz.bursifil.commanddrone.core.saveddevices.presentation.SavedDevicesPresenterImpl;
import cz.bursifil.commanddrone.core.saveddevices.presentation.SavedDevicesView;
import cz.bursifil.commanddrone.phone.saveddevices.view.SavedDevicesActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by filas on 3/6/2018.
 */
@Module
public abstract class SavedDevicesAssembly {

    @Provides
    static SavedDevicesPresenter provideSavedDevicesPresenter(SavedDevicesView savedDevicesView, SavedDevicesController savedDevicesController) {
        return new SavedDevicesPresenterImpl(savedDevicesView, savedDevicesController);
    }

    @Binds
    abstract SavedDevicesView provideSavedDevicesView(SavedDevicesActivity savedDevicesActivity);

    @Provides
    static SavedDevicesController provideSavedDevicesController() {
        return new SavedDevicesControllerImpl();
    }
}
