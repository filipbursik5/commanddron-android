package cz.bursifil.commanddrone.phone.main.view.Tabs;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.globalsettings.GlobalSettings;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.main.domain.Command;
import cz.bursifil.commanddrone.core.main.presentation.command.CommandPresenter;
import cz.bursifil.commanddrone.phone.platform.StringService;

/**
 * Created by filas on 8/28/2017.
 */

public class CommandsAdapter extends RecyclerView.Adapter<CommandsAdapter.ViewHolder> {

    private Activity activity;
    private List<Command> commands;
    private CommandPresenter commandPresenter;
    private StringService stringService;

    public CommandsAdapter(Activity activity, List<Command> commands, CommandPresenter commandPresenter, StringService stringService) {
        this.activity = activity;
        this.commands = commands;
        this.stringService = stringService;
        this.commandPresenter = commandPresenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_command, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.name.setText(commands.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return commands.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startCommand();
                }
            });
        }

        private void startCommand(){

            switch (commands.get(getAdapterPosition()).getName()){
                case "goTo":
                    FragmentTransaction ft = activity.getFragmentManager().beginTransaction();

                    ft.addToBackStack(null);

                    GotoFragmentDialog newFragment = GotoFragmentDialog.newInstance();
                    newFragment.setStringService(stringService);
                    newFragment.setListener(new GoToListener() {
                        @Override
                        public void goTo(String x, String y, String z, String tilt) {
                            commandPresenter.goTo(new Double(new Double(x) - GlobalSettings.X_OFFEST).toString(),
                                    new Double(new Double(y) - GlobalSettings.Y_OFFSET).toString(),z,tilt);
                            activity.onBackPressed();
                        }

                        @Override
                        public void goToAltitude(String altitude) {

                        }
                    });

                    newFragment.show(ft, "dialog");
                    break;
            }
        }
    }
}
