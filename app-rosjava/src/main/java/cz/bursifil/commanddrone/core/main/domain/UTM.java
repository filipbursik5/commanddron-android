package cz.bursifil.commanddrone.core.main.domain;

import com.example.globalsettings.GlobalSettings;

/**
 * Created by filas on 4/14/2018.
 */

public class UTM {

    private double x;
    private double y;
    private double z;

    public UTM(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x - GlobalSettings.X_OFFEST;
    }

    public double getY() {
        return y - GlobalSettings.Y_OFFSET;
    }

    public double getZ() {
        return z;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setZ(double z) {
        this.z = z;
    }
}
