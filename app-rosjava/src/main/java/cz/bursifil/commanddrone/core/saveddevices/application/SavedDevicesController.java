package cz.bursifil.commanddrone.core.saveddevices.application;

import java.util.List;

import cz.bursifil.commanddrone.core.PresenterCallback;
import cz.bursifil.commanddrone.core.saveddevices.domain.Device;

/**
 * Created by filas on 3/6/2018.
 */

public interface SavedDevicesController {
    void deleteDevice(int position);
    void connectDevice(Device device, PresenterCallback presenterCallback);
    List<Device> getDevices();
}
