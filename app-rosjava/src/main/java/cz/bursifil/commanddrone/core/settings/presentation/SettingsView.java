package cz.bursifil.commanddrone.core.settings.presentation;

import cz.bursifil.commanddrone.core.settings.domain.SettingsStrings;

/**
 * Created by filas on 3/6/2018.
 */

public interface SettingsView {
    void setupData(SettingsStrings settingsStrings);
    void setConnected(String text);
    void setDisconnected(String text);
    void setErrorConnection(String text);
}
