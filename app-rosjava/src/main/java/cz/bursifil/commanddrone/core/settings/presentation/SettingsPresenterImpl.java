package cz.bursifil.commanddrone.core.settings.presentation;

import javax.inject.Inject;

import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.PresenterCallback;
import cz.bursifil.commanddrone.core.settings.application.SettingsController;
import cz.bursifil.commanddrone.phone.root.DronApplication;

/**
 * Created by filas on 3/6/2018.
 */

public class SettingsPresenterImpl implements SettingsPresenter, PresenterCallback
{
    private SettingsView view;
    private SettingsController settingsController;

    private String username;
    private String hostname;
    private String password;

    @Inject
    public SettingsPresenterImpl(SettingsView settingsView, SettingsController settingsController) {
        this.view = settingsView;
        this.settingsController = settingsController;
    }

    public void onCreate(){
        if (settingsController.isConnected()){
            view.setConnected(DronApplication.getsContext().getString(R.string.connected));
        }else{
            view.setDisconnected(DronApplication.getsContext().getString(R.string.notconnected));
        }
    }

    @Override
    public void disconnect(){
        settingsController.disconnect();
    }

    @Override
    public void saveDevice() {
        settingsController.saveDevice(hostname, username, password);
    }

    @Override
    public void saveCreditionals(String hostname, String password, String username) {
        this.hostname = hostname;
        this.password = password;
        this.username = username;

       settingsController.connect(hostname, username, password, this);
    }

    @Override
    public void success(String data) {
        view.setConnected(DronApplication.getsContext().getString(R.string.connected));
    }

    @Override
    public void error(String data) {
        view.setDisconnected(DronApplication.getsContext().getString(R.string.notconnected));
        view.setErrorConnection(DronApplication.getsContext().getString(R.string.connection_error));
    }
}
