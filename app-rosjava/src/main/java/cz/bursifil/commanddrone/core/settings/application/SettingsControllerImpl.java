package cz.bursifil.commanddrone.core.settings.application;

import android.util.Log;

import com.jcraft.jsch.JSchException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import cz.bursifil.commanddrone.core.PresenterCallback;
import cz.bursifil.commanddrone.core.saveddevices.domain.Device;
import cz.bursifil.commanddrone.phone.root.DronApplication;
import cz.cvut.fel.androidsshcommunication.sshutils.ConnectionStatusListener;
import cz.cvut.fel.androidsshcommunication.sshutils.SessionController;

/**
 * Created by filas on 3/6/2018.
 */

public class SettingsControllerImpl implements SettingsController, ConnectionStatusListener {

    private final String FILENAME = "devices.dronapp";
    private final String CONNECTION_TAG = "CONNECTION";

    private PresenterCallback presenterCallback;

    public SettingsControllerImpl() {
    }

    @Override
    public void saveDevice(String hostname, String username, String password) {

        Device device = new Device(username, hostname, password);

        List<Device> devices = readFromFile();
        devices.add(device);

        try {
            FileOutputStream fout = new FileOutputStream(new File(DronApplication.getsContext().getFilesDir(), FILENAME));
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(devices);
            oos.close();
        }
        catch (Exception e) { e.printStackTrace(); }
    }

    @Override
    public void disconnect() {
        try {
            if (SessionController.isConnected()) {
                SessionController.disconnect();
            }
        } catch (Throwable t) { //catch everything!
            Log.e(CONNECTION_TAG, "Disconnect exception " + t.getMessage());
        }
    }

    @Override
    public void connect(String hostname, String username, String password, PresenterCallback presenterCallback) {
        this.presenterCallback = presenterCallback;

        try {
            SessionController.connect(username, hostname, password, "22", this);
        } catch (JSchException e) {
            presenterCallback.error("");
        }
    }

    @Override
    public boolean isConnected() {
        if (SessionController.isConnected()){
            return true;
        }

        return false;
    }

    private List<Device> readFromFile(){
        List<Device> deviceList = new ArrayList<>();

        FileInputStream fin;

        try {
            fin = new FileInputStream(new File(DronApplication.getsContext().getFilesDir(), FILENAME));
            ObjectInputStream ois = new ObjectInputStream(fin);
            deviceList = (List<Device>) ois.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return deviceList;
    }

    @Override
    public void onDisconnected() {
        presenterCallback.error("");
    }

    @Override
    public void onConnected() {
        presenterCallback.success("");
    }
}
