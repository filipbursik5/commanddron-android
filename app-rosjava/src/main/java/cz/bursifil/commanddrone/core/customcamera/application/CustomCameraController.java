package cz.bursifil.commanddrone.core.customcamera.application;

import cz.bursifil.commanddrone.phone.main.view.MainRosView;
import cz.bursifil.commanddrone.rospubsub.SimplePublisherNode;

/**
 * Created by filas on 3/22/2018.
 */

public class CustomCameraController {

    private static boolean exectuing = false;
    private static SimplePublisherNode node3 = new SimplePublisherNode();

    public void executeFollowCommand(Double x, Double y, Double z){
        node3.x = x.toString();
        node3.y = y.toString();
        node3.z = z.toString();

        if(!exectuing){
            exectuing = true;
            MainRosView.nodeMainExecutor.execute(node3, MainRosView.nodeConfiguration);
        }
    }

    public void stop(){
        node3.stop();
    }
}
