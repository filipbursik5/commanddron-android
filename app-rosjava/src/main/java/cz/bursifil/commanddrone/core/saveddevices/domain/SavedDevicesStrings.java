package cz.bursifil.commanddrone.core.saveddevices.domain;

/**
 * Created by filas on 3/6/2018.
 */

public class SavedDevicesStrings {

    public String navBartitle;

    public SavedDevicesStrings(String navBartitle) {
        this.navBartitle = navBartitle;
    }

    public String getNavBartitle() {
        return navBartitle;
    }

    public void setNavBartitle(String navBartitle) {
        this.navBartitle = navBartitle;
    }
}
