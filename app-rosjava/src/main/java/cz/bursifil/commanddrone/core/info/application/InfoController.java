package cz.bursifil.commanddrone.core.info.application;

import cz.bursifil.commanddrone.rospubsub.BatteryCallback;

/**
 * Created by filas on 3/6/2018.
 */

public interface InfoController {
    void getBattery(BatteryCallback batteryCallback);
}
