package cz.bursifil.commanddrone.core.main.presentation.command;

import com.jcraft.jsch.JSchException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.main.domain.Command;
import cz.bursifil.commanddrone.phone.main.view.MainRosView;
import cz.bursifil.commanddrone.phone.root.DronApplication;
import cz.bursifil.commanddrone.rospubsub.SimplePublisherNode;
import cz.bursifil.commanddrone.rospubsub.SimplePublisherNodeGoTo;
import cz.cvut.fel.androidsshcommunication.sshutils.CommandController;

/**
 * Created by filas on 3/6/2018.
 */

public class CommandPresenterImpl implements CommandPresenter
{
    private CommandView view;

    @Inject
    public CommandPresenterImpl(CommandView commandView) {
        this.view = commandView;
    }

    public void onCreate(){
        view.showCommands(getCommands());
    }

    @Override
    public void goTo(String x, String y, String z, String tilt) {

        SimplePublisherNodeGoTo node3 = new SimplePublisherNodeGoTo(x,y,z);
        MainRosView.nodeMainExecutor.execute(node3, MainRosView.nodeConfiguration);
    }

    public List<Command> getCommands(){
        List<Command> commandList = new ArrayList<>();
        commandList.add(new Command("goTo", "rosservice call /uav1/trackers_manager/mpc_tracker/goTo "));

        return commandList;
    }
}
