package cz.bursifil.commanddrone.core.main.presentation;

import cz.bursifil.commanddrone.core.main.domain.MainStrings;

/**
 * Created by filas on 3/6/2018.
 */

public interface MainView {
    void setupData(MainStrings mainStrings);
}
