package cz.bursifil.commanddrone.core.main.presentation.follow;

/**
 * Created by filas on 3/6/2018.
 */

public interface FollowPresenter {
    void onCreate();
    void follow();
    void setBearing(Double bearing);
    void setValuesAccelerometr(float[] mValuesAccelerometr);
    void setValuesMagnetic(float[] mValuesMagnetic);
    void setSeekBarProgress(int seekBarProgress);
}

