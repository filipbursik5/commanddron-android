package cz.bursifil.commanddrone.core.saveddevices.application;

import com.jcraft.jsch.JSchException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import cz.bursifil.commanddrone.core.PresenterCallback;
import cz.bursifil.commanddrone.core.saveddevices.domain.Device;
import cz.bursifil.commanddrone.phone.root.DronApplication;
import cz.cvut.fel.androidsshcommunication.sshutils.SessionController;

/**
 * Created by filas on 3/6/2018.
 */

public class SavedDevicesControllerImpl implements SavedDevicesController {

    private final String FILENAME = "devices.dronapp";

    @Override
    public void deleteDevice(int position) {
        List<Device> devices = readFromFile();

        devices.remove(position);

        try {
            FileOutputStream fout = new FileOutputStream(new File(DronApplication.getsContext().getFilesDir(), FILENAME));
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(devices);
            oos.close();
        }
        catch (Exception e) { e.printStackTrace(); }
    }

    @Override
    public void connectDevice(Device device, PresenterCallback presenterCallback) {
        try {
            SessionController.connect(device.getUsername(), device.getHostname(), device.getPassword(), "22", null);
        } catch (JSchException e) {
            presenterCallback.error("");
        }
    }

    @Override
    public List<Device> getDevices() {
        return readFromFile();
    }

    private List<Device> readFromFile(){
        List<Device> deviceList = new ArrayList<>();

        FileInputStream fin;

        try {
            fin = new FileInputStream(new File(DronApplication.getsContext().getFilesDir(), FILENAME));
            ObjectInputStream ois = new ObjectInputStream(fin);
            deviceList = (List<Device>) ois.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return deviceList;
    }
}
