package cz.bursifil.commanddrone.core.main.presentation.follow;

import android.hardware.SensorManager;
import android.location.Location;
import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import cz.bursifil.commanddrone.R;
import cz.bursifil.commanddrone.core.PresenterCallback;
import cz.bursifil.commanddrone.core.main.application.follow.FollowController;
import cz.bursifil.commanddrone.core.main.domain.UTM;
import cz.bursifil.commanddrone.phone.main.view.MainRosView;
import cz.bursifil.commanddrone.phone.platform.GPSUtility;
import cz.bursifil.commanddrone.phone.root.DronApplication;

/**
 * Created by filas on 3/6/2018.
 */

public class FollowPresenterImpl implements FollowPresenter, PresenterCallback
{
    private static final Double ALLOWED_DISTANCE = 5.0;
    private FollowView view;

    private boolean followingInd = false;
    private Double bearing;
    private int seekBarProgress;

    private final Double[] A = new Double[1];
    private final Double[] B = new Double[1];
    private final Double[] C = new Double[1];

    final float[] mValuesMagnet = new float[3];
    final float[] mValuesAccel = new float[3];
    final float[] mValuesOrientation = new float[3];
    final float[] mRotationMatrix = new float[9];
    private Timer timerTask = new Timer();
    private Double myHeigh = 3.0;

    private FollowController controller;
    private GPSUtility gpsUtility;
    private Double distanceBetweenLastPositions = 0.0;
    private Location lastLocationc;
    private Location myLocation;
    private Double alpha;
    private Double beta;

    private boolean saving = false;

    @Inject
    public FollowPresenterImpl(FollowView followView, FollowController followController, GPSUtility gpsUtility) {
        this.view = followView;
        this.controller = followController;
        this.gpsUtility = gpsUtility;
    }

    public void onCreate(){
    }

    public void setSeekBarProgress(int seekBarProgress) {
        this.seekBarProgress = seekBarProgress;
    }

    @Override
    public void setBearing(Double bearing) {
        this.bearing = bearing;
    }

    @Override
    public void setValuesAccelerometr(float[] mValuesAccelerometr) {
        System.arraycopy(mValuesAccelerometr, 0, mValuesAccel, 0, 3);
    }

    @Override
    public void setValuesMagnetic(float[] mValuesMagnetic) {
        System.arraycopy(mValuesMagnetic, 0, mValuesMagnet, 0, 3);
    }

    @Override
    public void follow(){
        if(followingInd){

            followingInd = false;

            view.setNotFollowing(DronApplication.getsContext().getString(R.string.start_follow));

            timerTask.cancel();
            timerTask.purge();
            controller.stop();
        }else{
            followingInd = true;
            timerTask = new Timer();

            view.setFollowing(DronApplication.getsContext().getString(R.string.stopfollowing));

            timerTask.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        float[] temp = new float[9];
                        float[] RR = new float[9];
                        float[] orientation = new float[3];
                        float[] rotation = new float[9];

                        float[] mOriginValuesAccel = mValuesAccel.clone();
                        float[] mOriginValuesMagnet = mValuesMagnet.clone();

                        SensorManager.getRotationMatrix(rotation, null, mOriginValuesAccel, mOriginValuesMagnet);
                        SensorManager.getOrientation(rotation, orientation);

                        SensorManager.getRotationMatrix(temp, null,
                                mOriginValuesAccel, mOriginValuesMagnet);

                        SensorManager.remapCoordinateSystem(temp,
                                SensorManager.AXIS_Y,
                                SensorManager.AXIS_Z, RR);

                        float[] values = new float[3];
                        SensorManager.getOrientation(RR, values);

                        Double degrees = (values[2] * 180) / Math.PI;

                        if (degrees < 0) {
                            degrees += 360;
                        }

                        degrees -= 270;

                        if (degrees < 0) {
                            degrees += 360;
                        }

                        if (bearing < 0) {
                            bearing += 360;
                        }

                        beta = Math.PI / 180 * degrees;
                        alpha = Math.PI / 180 * bearing;

                        A[0] = seekBarProgress * Math.cos(beta) * Math.cos(alpha);
                        C[0] = seekBarProgress * Math.sin(beta);
                        B[0] = seekBarProgress * Math.sin(alpha) * Math.cos(beta);

                        myLocation = gpsUtility.getLocation();

                        if (lastLocationc != null) {
                            distanceBetweenLastPositions =
                                    (new Double(myLocation.distanceTo(lastLocationc))); // + distanceBetweenLastPositions) / 2;

                            if (distanceBetweenLastPositions > ALLOWED_DISTANCE) {
                                //lastLocationc = myLocation;
                                return;
                            }
                        }

                        Location movedLocation = gpsUtility.moveGps(bearing, seekBarProgress, myLocation);
                        view.addMarker(movedLocation);
                        UTM utmCoordinates = gpsUtility.Deg2UTM(movedLocation);
                        utmCoordinates.setZ(myHeigh + Math.sin(beta) * seekBarProgress);

                        if(utmCoordinates.getZ() < 0){
                            return;
                        }

                        view.setInfo(degrees.toString(), bearing.toString(), new Double(utmCoordinates.getX()).toString(),
                                new Double(utmCoordinates.getY()).toString(), new Double(utmCoordinates.getZ()).toString());

                        controller.executeFollowCommand(utmCoordinates.getX(), utmCoordinates.getY(), utmCoordinates.getZ(), A[0], B[0], C[0], myLocation);
                        //controller.executeFollowCommand(A[0], B[0], C[0]);
                    }
            }, 1000, 300);
        }
    }

    @Override
    public void success(String data) {

    }

    @Override
    public void error(String data) {
        followingInd = false;

        view.setNotFollowing(DronApplication.getsContext().getString(R.string.start_follow));
        view.followingError(DronApplication.getsContext().getString(R.string.wrong));
    }
}
