package cz.bursifil.commanddrone.core.main.application.follow;

import android.location.Location;

import com.jcraft.jsch.JSchException;

import java.io.IOException;

import cz.bursifil.commanddrone.core.PresenterCallback;
import cz.bursifil.commanddrone.phone.main.view.MainRosView;
import cz.bursifil.commanddrone.rospubsub.SimplePublisherNode;
import cz.cvut.fel.androidsshcommunication.sshutils.CommandController;

/**
 * Created by filas on 3/22/2018.
 */

public class FollowController {

    private static boolean exectuing = false;
    private static SimplePublisherNode node3 = new SimplePublisherNode();

    public void executeFollowCommand(Double x, Double y, Double z, Double x_vector, Double y_vector, Double z_vector, Location myLocation){
        node3.x = x.toString();
        node3.y = y.toString();
        node3.z = z.toString();

        node3.x_vector = x_vector.toString();
        node3.y_vector = y_vector.toString();
        node3.z_vector = z_vector.toString();

        node3.myLocation = myLocation;

        if(!exectuing){
            exectuing = true;
            MainRosView.nodeMainExecutor.execute(node3, MainRosView.nodeConfiguration);
        }
    }

    public void stop(){
        node3.stop();
    }
}
