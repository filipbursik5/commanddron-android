package cz.bursifil.commanddrone.core;

/**
 * Created by filipbursik on 30.03.18.
 */

public interface PresenterCallback {
    void error(String text);
    void success(String text);
}
