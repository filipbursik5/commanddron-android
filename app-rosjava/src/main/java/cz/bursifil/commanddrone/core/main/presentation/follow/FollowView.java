package cz.bursifil.commanddrone.core.main.presentation.follow;

import android.location.Location;

import cz.bursifil.commanddrone.core.main.domain.MainStrings;

/**
 * Created by filas on 3/6/2018.
 */

public interface FollowView {
    void setupData(MainStrings mainStrings);
    void setFollowing(String text);
    void setNotFollowing(String text);
    void followingError(String text);
    void setInfo(String alpha, String beta, String x, String y, String z);
    void addMarker(Location location);
}
