package cz.bursifil.commanddrone.core.saveddevices.presentation;

import javax.inject.Inject;

import cz.bursifil.commanddrone.core.PresenterCallback;
import cz.bursifil.commanddrone.core.saveddevices.application.SavedDevicesController;
import cz.bursifil.commanddrone.core.saveddevices.domain.Device;

/**
 * Created by filas on 3/6/2018.
 */

public class SavedDevicesPresenterImpl implements SavedDevicesPresenter, PresenterCallback
{
    private SavedDevicesView view;
    private SavedDevicesController savedDevicesController;

    @Inject
    public SavedDevicesPresenterImpl(SavedDevicesView savedDevicesView, SavedDevicesController savedDevicesController) {
        this.savedDevicesController = savedDevicesController;
        this.view = savedDevicesView;
    }

    public void onCreate(){
        view.showDevices(savedDevicesController.getDevices());
    }

    public void startConnecting(Device device){
        savedDevicesController.connectDevice(device, this);
    }

    public void deleteDevice(Integer position){
        savedDevicesController.deleteDevice(position);
    }

    @Override
    public void success(String data) {

    }

    @Override
    public void error(String data) {

    }
}

