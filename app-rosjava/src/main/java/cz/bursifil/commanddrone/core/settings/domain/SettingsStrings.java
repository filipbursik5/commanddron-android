package cz.bursifil.commanddrone.core.settings.domain;

/**
 * Created by filas on 3/6/2018.
 */

public class SettingsStrings {

    public String navBartitle;

    public SettingsStrings(String navBartitle) {
        this.navBartitle = navBartitle;
    }

    public String getNavBartitle() {
        return navBartitle;
    }

    public void setNavBartitle(String navBartitle) {
        this.navBartitle = navBartitle;
    }
}
