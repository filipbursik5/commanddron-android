package cz.bursifil.commanddrone.core.info.application;

import com.jcraft.jsch.JSchException;

import org.ros.node.NodeMain;

import java.io.IOException;

import cz.bursifil.commanddrone.core.PresenterCallback;
import cz.bursifil.commanddrone.phone.main.view.MainRosView;
import cz.bursifil.commanddrone.rospubsub.BatteryCallback;
import cz.bursifil.commanddrone.rospubsub.SimpleSubscriberNode;
import cz.cvut.fel.androidsshcommunication.sshutils.CommandController;

/**
 * Created by filas on 3/6/2018.
 */

public class InfoControllerImpl implements InfoController {

    @Override
    public void getBattery(BatteryCallback batteryCallback) {
        NodeMain node2 = new SimpleSubscriberNode(batteryCallback);
        MainRosView.nodeMainExecutor.execute(node2, MainRosView.nodeConfiguration);
    }
}
