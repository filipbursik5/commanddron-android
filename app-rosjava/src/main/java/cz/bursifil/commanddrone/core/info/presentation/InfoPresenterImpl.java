package cz.bursifil.commanddrone.core.info.presentation;

import javax.inject.Inject;

import cz.bursifil.commanddrone.core.PresenterCallback;
import cz.bursifil.commanddrone.core.info.application.InfoController;
import cz.bursifil.commanddrone.rospubsub.Battery;
import cz.bursifil.commanddrone.rospubsub.BatteryCallback;

/**
 * Created by filas on 3/6/2018.
 */

public class InfoPresenterImpl implements InfoPresenter, BatteryCallback
{
    private InfoView view;
    private InfoController infoController;

    @Inject
    public InfoPresenterImpl(InfoView infoView, InfoController infoController) {
        this.view = infoView;
        this.infoController = infoController;
    }

    public void onCreate(){
       infoController.getBattery(this);
    }

    @Override
    public void returnBattery(Battery battery) {
        view.showVoltage(battery.getVoltage());
        view.showBattery(battery.getPercetange());
    }
}
