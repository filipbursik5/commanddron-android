package cz.bursifil.commanddrone.core.saveddevices.presentation;

import java.util.List;

import cz.bursifil.commanddrone.core.main.domain.MainStrings;
import cz.bursifil.commanddrone.core.saveddevices.domain.Device;

/**
 * Created by filas on 3/6/2018.
 */

public interface SavedDevicesView {
    void setupData(MainStrings mainStrings);
    void showDevices(List<Device> devices);
}
