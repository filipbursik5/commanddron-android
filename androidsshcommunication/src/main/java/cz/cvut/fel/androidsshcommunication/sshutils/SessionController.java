package cz.cvut.fel.androidsshcommunication.sshutils;

import android.util.Log;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

/**
 * Created by filipbursik on 30.03.18.
 */

public class SessionController {

    private static Session session;

    public static void connect(final String username, final String hostname, final String password, final String port, final ConnectionStatusListener connectionStatusListener) throws JSchException {
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try  {
                    JSch jsch = new JSch();
                    Session session = jsch.getSession(username, hostname, Integer.valueOf(port));
                    session.setPassword(password);
                    java.util.Properties config = new java.util.Properties();
                    config.put("StrictHostKeyChecking", "no");
                    session.setConfig(config);
                    session.setTimeout(10000);
                    session.connect();

                    if(connectionStatusListener != null) {
                        connectionStatusListener.onConnected();
                    }

                    SessionController.session = session;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
    }

    public static void disconnect(){
        session.disconnect();
    }

    public static boolean isConnected(){
        if(session == null) return false;
        return session.isConnected();
    }

    public static Session getSession() {
        return session;
    }
}
