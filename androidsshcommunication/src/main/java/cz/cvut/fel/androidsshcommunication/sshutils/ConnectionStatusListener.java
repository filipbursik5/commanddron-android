package cz.cvut.fel.androidsshcommunication.sshutils;

/**
 * Created by filipbursik on 30.03.18.
 */

public interface ConnectionStatusListener {
    void onDisconnected();
    void onConnected();
}
