package cz.cvut.fel.androidsshcommunication.sshutils;

import android.util.Log;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by filipbursik on 30.03.18.
 */

public class CommandController {

    public static void executeCommand(final String command, final CommandCallback commandCallback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                ChannelExec channelssh = null;

                try {

                    long currentTime = System.currentTimeMillis();

                    channelssh = (ChannelExec)
                            SessionController.getSession().openChannel("exec");


                    InputStream in = channelssh.getInputStream();

                    channelssh.setCommand(command);
                    channelssh.connect();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    String line;
                    String result = "";

                    while ((line = reader.readLine()) != null) {
                        result += line + "\n";
                    }

                    Log.e("TIME", new Long(System.currentTimeMillis() -  currentTime).toString());

                    commandCallback.setResult(result);
                } catch (JSchException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

}
