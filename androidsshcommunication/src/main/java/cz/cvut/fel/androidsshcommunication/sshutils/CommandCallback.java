package cz.cvut.fel.androidsshcommunication.sshutils;

public interface CommandCallback {
    void setResult(String result);
}
